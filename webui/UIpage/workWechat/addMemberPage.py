# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:  页面功能

from webui.common.commonTools import CommonTools
from webui.common.webUI_basic_operation import BasicOperation

data = CommonTools().get_test_data("workWechat")


class AddMemberPage(BasicOperation):
    username = data["addMemberPage"]["username"]
    jincheng = data["addMemberPage"]["jincheng"]
    bianhao = data["addMemberPage"]["bianhao"]
    phone = data["addMemberPage"]["phone"]
    baocun = data["addMemberPage"]["baocun"]

    def add_member(self, name, jiancheng, bianhao, phone):
        """
        添加成员功能
        :return:
        """
        self.send_keys(self.username, name)
        self.send_keys(self.jincheng, jiancheng)
        self.send_keys(self.bianhao, bianhao)
        self.send_keys(self.phone, phone)
        self.click(self.baocun)
        from webui.UIpage.workWechat.contactsPage import ContactsPage
        return ContactsPage(self.driver)
