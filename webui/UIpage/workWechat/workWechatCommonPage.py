# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:  页面功能

from webui.common.webUI_basic_operation import BasicOperation


class WorkWechatCommonPage(BasicOperation):
    """
    每个业务有自己的公共复用页面，比如自己业务的登录，退出，导航栏切换等。复用率高的放这里。
    """

    def login(self):
        return
