# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:  页面功能

from webui.common.commonTools import CommonTools
import time
from webui.common.webUI_basic_operation import BasicOperation

data = CommonTools().get_test_data("workWechat")


class ContactsPage(BasicOperation):
    tianjiachengyuan = data["contactsPage"]["tianjiachengyuan"]
    namedriver = data["contactsPage"]["namedriver"]

    def go_to_addMemberPage(self):
        """
        跳转添加成员页面功能
        :return: 返回添加成员页面
        """

        time.sleep(3)
        self.click(self.tianjiachengyuan)

        from webui.UIpage.workWechat.addMemberPage import AddMemberPage
        # return self  返回本身，就可以调用自己了
        return AddMemberPage(self.driver)

    def get_contact_list(self):
        """
        :return: 返回断言
        """
        name = []

        time.sleep(6)
        # 手撕定位，.是代表clss属性=。：后面是第二个孩子
        namedrivers = self.find_elements(self.namedriver)
        for namedriver in namedrivers:
            name.append(namedriver.text)
        # 下面的循环取等同于，上面
        # num=self.driver.find_element_by_id("member_list").find_elements_by_tag_name("tr")
        # for i in  range(1,len(num)+1):
        #     name=self.driver.find_element_by_xpath(f"//*[@id='member_list']/tr[{i}]/td[2]").text
        #     name.append(name)
        return name
