# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:  页面功能


from webui.common.webUI_basic_operation import BasicOperation
from webui.common.commonTools import CommonTools
import time

data = CommonTools().get_test_data("workWechat")


class HomePage(BasicOperation):
    contact = data["homePage"]["contact"]
    gotoaddmember = data["homePage"]["gotoaddmember"]

    def go_to_contactsPage(self):
        """
        跳转到通讯录功能
        :return: 通讯录页面对象
        """
        time.sleep(5)
        # 继承基类，基类的init方法直接实例化了driver给了，self.driver，子类直接调用
        self.click(self.contact)
        # 需要在这里导，直接在外面导，会提示，循环导入的问题，用到再导入
        from webui.UIpage.workWechat.contactsPage import ContactsPage
        return ContactsPage(self.driver)

    def go_to_addMemberPage(self):
        """
        跳转添加成员页面功能
        :return: 成员页面对象
        """
        self.click(self.gotoaddmember)
        # 为了传递driver，返回实例对象，就会走该类的init方法，该类没有就走，父类的init
        # 而父类的init判断了，driver为none新建，为none就传递，这样就保证一个driver实例对象传递的
        from webui.UIpage.workWechat.addMemberPage import AddMemberPage
        return AddMemberPage(self.driver)
