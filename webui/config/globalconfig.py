# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:      全局静态常量或者变量，供全局调用。分布式时，可以写出key value的形式

class GlobalConfig:

    DRIVER=None

