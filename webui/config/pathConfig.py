# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:      路径层 ,路径管理层

import os



BASE_PATH=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
REPRORPATH=os.path.join(BASE_PATH,"report","allurereport")
TESTCASEPATH = os.path.join(BASE_PATH, "testcase")
IMAGEPATH=os.path.join(BASE_PATH,"report","images")
LOGCONFIGPATH=os.path.join(BASE_PATH,'config','logconfig.ini')
LOGSPATH=os.path.join(BASE_PATH,'report','logs')
DATAPATH = os.path.join(BASE_PATH, "data")
UIPAGEPATH= os.path.join(BASE_PATH, "UIpage")
TEMPLATEPATH=os.path.join(BASE_PATH,"template")
APIPATH=os.path.join(TEMPLATEPATH,"api_template")
DBSQLPATH=os.path.join(TEMPLATEPATH,"db_sql_template")
CASETMEPLATEPATH=os.path.join(TEMPLATEPATH,"case_template")
ELEMENTPATH=os.path.join(TEMPLATEPATH,"element_template.yml")
TESTPARAMDATAPATH=os.path.join(TEMPLATEPATH,"testparamData_template.yml")









