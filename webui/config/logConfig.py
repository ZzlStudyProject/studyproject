# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:    日志配置文件

#todo  主要节点，还没有开始加日志输出
import logging
import logging.config
import datetime
from webui.common.commonTools import CommonTools
from webui.config.pathConfig import LOGCONFIGPATH
from webui.config.pathConfig import LOGSPATH


nowtime = datetime.datetime.now().strftime('%Y_%m_%d_%H')  # 格式化时间
args=str((LOGSPATH+"\\"+nowtime+".log",'midnight',10,40))

class Logger():
    def __init__(self):
        CommonTools().changeini(LOGCONFIGPATH,"handler_fileHandler","args",args)
        logging.config.fileConfig(fname=LOGCONFIGPATH)
        self.loger = logging.getLogger("uilog");

    def get_logger(self):
        return self.loger





