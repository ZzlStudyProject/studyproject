# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:    数据库公共底层配置。 当发现不够用，或者查询类型不够用时，优化封装，丰富分类，比如不光按照环境分数据库 还按照项目或者ip地址分类


import pymysql


class DateBase:
    """
    数据库连接的基类，直接把数据库参数写在这里，包装查询方法
    后续其他人写自己业务时，直接集成调用
    """



    def __init__(self,env=None):
        """
        #根据环境选择库，当然库多，跨库，增加标识.库参数直接写死，因为就是config文件
        :param env:
        """
        try:
            db_config_defult = {
                "host": "127.0.0.1",
                "port": 3306,
                "user": "root",
                "passwd": "zhangzhenglai"
            }
            db_config_dev={
                "host": "127.0.0.1",
                "port": 3306,
                "user": "root",
                "passwd": "zhangzhenglai"
            }
            db_config_test = {
                "host": "127.0.0.1",
                "port": 3306,
                "user": "root",
                "passwd": "zhangzhenglai"
            }
            if env=="dev":
                coon = pymysql.connect(**db_config_dev)
                self.cur = coon.cursor(pymysql.cursors.DictCursor)
            elif env=="test":
                coon = pymysql.connect(**db_config_test)
                self.cur = coon.cursor(pymysql.cursors.DictCursor)
            elif env==None:
                coon = pymysql.connect(**db_config_defult)
                self.cur = coon.cursor(pymysql.cursors.DictCursor)
        except Exception as e:
            print("数据库连接出问题了",str(e))

    def query(self,sql):
        """
        查询语句，返回结果
        :param sql:
        :return:
        """
        try:
            self.cur.execute(sql)
            return self.cur.fetchall() #返回执行结果
        except Exception as e:
            print('sql语句有误！%s' % e)
        finally:
            self.cur.close()



    def table_exists(self, database_name, table_name):
        sql = f"SELECT table_name FROM {database_name}.TABLES WHERE table_name ='{table_name}';"
        if self.query(sql):
            return True










if __name__ == '__main__':
    print()

