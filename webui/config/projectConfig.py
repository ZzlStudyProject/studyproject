# -*- coding: utf-8 -*-
# @CreateTime    : 2021/7/12
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/7/12 16:57
# @UpdateAuthor  : zhangzhenglai
# @Function:   项目配置，新项目，直接在这里加项目名称，用于用例生成。暂时先用简单的list管理。项目没那么多。多时再考虑管理模式问题。


PROJECTNAME=["projectdemo01"]