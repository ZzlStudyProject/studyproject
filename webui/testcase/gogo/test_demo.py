# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:

import allure
from webui.UIpage.workWechat.homePage import HomePage
import pytest
from webui.common.commonTools import CommonTools
from webui.common.browser_engine import BrowserDriver
data = CommonTools().get_test_data(path='workWechat', file='testparamData.yml')
fakerRamdomData = CommonTools().faker_random()




@allure.feature("web端ui自动化")
class TestDemo:

    def setup_method(self):
        self.driver = BrowserDriver().open_brwose(url=data['URL']['url0'])

    def teardown_method(self):
        self.driver.close()
        print("结束了")


    @allure.story("案例1")
    # 参数化，数据来源是多处的，1.yml文件，2，数据库，3，api接口
    @pytest.mark.parametrize("assertname,name,jiancheng,bianhao,phone", [[
        fakerRamdomData['personName'], fakerRamdomData['personName'], data['testData']['jiancheng'],
         data['testData']['bianhao'], fakerRamdomData["phoneNumber"]
    ]
    ])
    def test_demo(self, assertname, name, jiancheng, bianhao, phone):
        #         1.主页点击去通讯录页，2.通讯录页取添加成员页，3添加成员页添加成员，4.通讯录页返回成员列表
        names = HomePage(self.driver).go_to_contactsPage().go_to_addMemberPage().add_member(name, jiancheng, bianhao,
                                                                                            phone).get_contact_list()
        assert names[0] == assertname


