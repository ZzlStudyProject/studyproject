# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function: 主函数入口，执行文件


#todo 支持选环境参数。用例参数化时最好也支持下环境参数。或者加标签。跑线上的用例

import pytest
import os
from webui.config.pathConfig import REPRORPATH
from webui.config.pathConfig import TESTCASEPATH

class Run:

    def run_case(self, path=None, test_case=None):
        """
        用例运行，支持3种运行方式
        1.不传参时，全量运行testcase文件夹下所有用例Run().run_case()
        2.指定文件夹时，支持 运行该文件夹下所有用例  Run().run_case(["gogo","workWechat"])
        3.支持运行指定py文件的用例。 Run().run_case(["gogo"],['test_demo.py','test_demo2.py'])
        :param path:文件夹名称， testcase下文件夹名称
        :param test_case: 文件夹下   py用例
        :return:
        """

        runlist = ["-s", "-v", "--alluredir", REPRORPATH]
        cmd = "allure serve " + REPRORPATH
        if path == None:
            runlist.insert(2,TESTCASEPATH)
        elif type(path) == list and test_case==None:
            [runlist.insert(2,os.path.join(TESTCASEPATH,i)) for i in path]
        elif type(path) == list and len(path) == 1 and type(test_case) == list:
            [runlist.insert(2,os.path.join(TESTCASEPATH,path[0])+"\\"+i) for i in test_case]
        pytest.main(runlist)
        os.system(cmd)



if __name__ == '__main__':
    # Run().run_case(["gogo"],['test_demo.py','test_demo2.py'])
    Run().run_case()


