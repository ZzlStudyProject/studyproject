# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function: 浏览器引擎配置文件，ie，chrome，firefox，无页面，等这里配置


from selenium import webdriver
from webui.config.globalconfig import GlobalConfig




# todo 追加loggin，连接sonar
class BrowserDriver:


        def open_brwose(self, url, browser_type=None):

            """
            选择浏览器，打开url
            :param url: 打开的网址
            :param browser_type:选择的浏览器类型，默认chrome
            :return: 返回driver实例
            """
            if browser_type== "IE":
                self.driver=webdriver.Ie()
            elif browser_type== "firefox":
                self.driver=webdriver.Firefox()
            else:
                #实例化chrome的启动参数，有哪些参数，可以点进去看源码
                options = webdriver.ChromeOptions()
                #启动配置里面，添加配置，这里配置是屏蔽自动化监控的，你也可以追加别的，看需求。需求多了，看看有没有必要写成配置的
                #默认启动的是干净的chrome 而有时是需要带着插件启动的，这时候就需要配置
                options.add_argument('--ignore-certificate-errors') #忽略连接警告信息
                #把配置参数的实例传进去
                self.driver = webdriver.Chrome()
                # 加cookie,想加cookie必须先打开个url
            GlobalConfig.DRIVER = self.driver
            self.driver.get(url)
            self.driver.maximize_window()
            self.driver.implicitly_wait(10)
            return self.driver


        def open_mobile_browser(self, url, mobile_emulation={'deviceName': 'iPhone X'}):
            """
            #模拟手机浏览器，有的网址是手机版的，比如 m.xxx.com
            :param url:
            :param mobile_emulation: 手机参数，默认给了一个，应该是只能模拟苹果的，不过自动化本身就不是测兼容的
            :return: 返回driver实例
            """
            options = webdriver.ChromeOptions()
            options.add_experimental_option('mobile_emulation', mobile_emulation)  # 默认 模拟iPhone X ，也可以ipad
            options.add_argument('--ignore-certificate-errors')
            #w3c >= flase 问题，大概的意思就是，有时浏览器，启动session会话，返回是不符合w3c标准的
            #导致不能运行（加这个参数就回忽略可以使用，应该是再更新最新浏览器的时候可能会遇到）
            options.add_experimental_option('w3c', False)
            self.driver = webdriver.Chrome(options=options)
            self.driver.get(url)
            self.driver.maximize_window()
            self.driver.implicitly_wait(10)
            return self.driver


        def open_headless_browser(self, url, browser_type=None):
            """
            选择浏览器，打开url
            :param url: 打开的网址
            :param browser_type:选择的浏览器类型，默认chrome
            :return: 返回driver实例
            """
            if browser_type== "firefox":
                options=webdriver.FirefoxOptions()
                options.add_argument('headless')
                #看源码，firefox_options这个好像废弃了，写了也是强转成options
                self.driver=webdriver.Firefox(firefox_options=options)
            else:
                #实例化chrome的启动参数，有哪些参数，可以点进去看源码
                options = webdriver.ChromeOptions()
                #启动配置里面，添加配置，这里配置是屏蔽自动化监控的，你也可以追加别的，看需求。需求多了，看看有没有必要写成配置的
                #默认启动的是干净的chrome 而有时是需要带着插件启动的，这时候就需要配置
                options.add_argument('--ignore-certificate-errors') #忽略连接警告信息
                #看源码，设置无界面模式
                options.add_argument('headless')
                #把配置参数的实例传进去
                self.driver = webdriver.Chrome(options=options)
                # 加cookie,想加cookie必须先打开个url
            GlobalConfig.DRIVER = self.driver
            self.driver.get(url)
            self.driver.maximize_window()
            self.driver.implicitly_wait(10)
            return self.driver



        def quit_browser(self):
            self.driver.quit()



