# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:      把selenium基本操作当作原子操作进行做封装，相当于方法库，直接提供给他人写代码时，直接调用。发现方法不足时，再丰富追加，全局的谁都可以调用

import requests
import datetime
import time
from telnetlib import EC
from selenium.webdriver import TouchActions
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from webui.config.pathConfig import BASE_PATH
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from webui.config.pathConfig import IMAGEPATH



# todo  对细节没有做logger 日志输入，以及后续，原子方法还有很多没封装

class BasicOperation:

    def __init__(self,base_driver:webdriver=None):
        """
        每个page类都继承基类，那只要加载每个page对象，都会走基类的init方法，都会重新实例化一个driver
        每个page方法里面的return时，直接把driver，传递下去 如: return AddMemberPage(self.driver)
        解决，driver多次实例化，多次启动窗口问题
        init函数是不可以return的
        :param base_driver: base_driver:webdriver 这样的写法，是赋值类型，再赋值None 赋值类型含义，后面再调用时，.方法方便
        """
        if base_driver == None:
            options = webdriver.ChromeOptions()
            options.add_argument('--ignore-certificate-errors')
            self.driver = webdriver.Chrome(options=options)
            self.driver.implicitly_wait(10)
            self.driver = base_driver
        else:
            self.driver=base_driver


    #   添加cookie
    def add_cookies(self, cookie_dicts):
        """
        :param cookie_dicts: 传字典形式 key value
        :return:
        """
        self.driver.add_cookie(cookie_dicts)

    #下滑n页，在第二页想到第三页n的值也要填3，“页”的含义为当前屏幕大小的范围，n为3意味着下拉3个屏幕的范围
    def glide(self, n):
        """
        下滑n页，在第二页想到第三页n的值也要填3，“页”的含义为当前屏幕大小的范围，n为3意味着下拉3个屏幕的范围
        :param n:
        :return:
        """
        size = self.driver.get_window_size()
        y = size['height']
        y1 = y * (n + 1)
        win = "window.scrollTo(0," + str(y1) + ");"
        self.driver.execute_script(win)

    # 浏览器前进
    def browser_forward(self):
        self.driver.forward()

    # 浏览器后退
    def browser_back(self):
        self.driver.back()

    # 设置隐式等待时间
    def wait(self, seconds):
        self.driver.implicitly_wait(seconds)

    # 关闭当前窗口
    def close_window(self):
        self.driver.close()

    # 截图功能:得到截图并保存图片到项目image目录下
    def screenshot(self):
        nowtime = datetime.datetime.now().strftime('%Y%m%d%H%M%S')  # 格式化时间
        # 定义截图文件名称 前面的filePath是一定要写的，是存放路径。get_screenshot_as_file方法就这样的。识别路径存放
        image_name =IMAGEPATH + nowtime + '.png'
        #调用自带的截图函数
        self.driver.get_screenshot_as_file(image_name)
        return image_name


    #截网页全屏
    def fullscreenshot(self):
        """
        *注意：需要启动浏览器为无界面模式
        """
        nowtime = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
        image_name = IMAGEPATH + nowtime + '.png'
        width = self.driver.execute_script("return document.documentElement.scrollWidth")
        height = self.driver.execute_script("return document.documentElement.scrollHeight")
        self.driver.set_window_size(width, height)
        time.sleep(1)
        self.driver.save_screenshot(image_name)

    #封装定位方式
    def find_element(self, location):
        """
        你也可以封装成元组形式，你看下find底层就是元组，知识习惯了这么封装不爱使用元组
        :param location: 定位方式拼接，例： xpath=>html/table
        :return: element  把定位元素返回，用于后续操作
        """
        try:
            bytype=location.split("=>")[0]
            value=location.split("=>")[1]
            if bytype == "xpath":
                element = self.driver.find_element_by_xpath(value)
            elif bytype == "css":
                element = self.driver.find_element_by_css_selector(value)
            elif bytype == "id":
                element=self.driver.find_element_by_id(value)
            elif bytype == "name":
                element = self.driver.find_element_by_name(value)
            elif bytype == "class_name":
                element=self.driver.find_element_by_class_name(value)
            elif bytype =="link_text":
                element=self.driver.find_element_by_link_text(value)
            elif bytype == "tag_name":
                element=self.driver.find_element_by_tag_name(value)
            elif bytype == "partial_link_text":
                element=self.driver.find_element_by_partial_link_text(value)
        except Exception as e:
            print(e)
        return element

    #封装定位方式复数
    def find_elements(self, location):
        """
        :param location: 定位方式拼接，例： xpath=>html/table
        :return: element  把定位元素返回，用于后续操作
        """
        try:
            bytype=location.split("=>")[0]
            value=location.split("=>")[1]
            if bytype == "xpath":
                element = self.driver.find_elements_by_xpath(value)
            elif bytype == "css":
                element = self.driver.find_elements_by_css_selector(value)
            elif bytype == "id":
                element=self.driver.find_element_by_id(value)
            elif bytype == "name":
                element = self.driver.find_element_by_name(value)
            elif bytype == "class_name":
                element=self.driver.find_element_by_class_name(value)
            elif bytype =="link_text":
                element=self.driver.find_element_by_link_text(value)
            elif bytype == "tag_name":
                element=self.driver.find_element_by_tag_name(value)
            elif bytype == "partial_link_text":
                element=self.driver.find_element_by_partial_link_text(value)
        except Exception as e:
            print("请检查定位方式是否输入正确"+e)
        return element

    #显示等待元素是否存在（还可扩展其他的）
    def wait_element(self, selector, secs=5):
        """
        显示等待元素，presence_of_element_located 是检查页面是否存在元素，你还可以扩展其他的
        比如 visibility_of_element_located检查元素是否存在以及元素是否可见。
        是否可点击，是否可输入等等。发现不够用可以补充
        :param selector:定位方式
        :param secs:超时时间，后面的参数是巡检间隔时间，写个默认1秒循环检查一次
        :return:
        """
        if "=>" not in selector:
            raise NameError("请检查元素写法是否正确 '=>'.")

        by = selector.split("=>")[0]
        value = selector.split("=>")[1]

        if by == "id":
            WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.ID, value)))
        elif by == "name":
            WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.NAME, value)))
        elif by == "class_name":
            WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.CLASS_NAME, value)))
        elif by == "link_text":
            WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.LINK_TEXT, value)))
        elif by == "partial_link_text":
            WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, value)))
        elif by == "xpath":
            WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.XPATH, value)))
        elif by == "css":
            WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.CSS_SELECTOR, value)))
        else:
            raise NameError(
                "检查是否前缀错误了,'id','name','class','link_text','xpath','css','partial_link_text'")


    # 封装输入框方法
    def send_keys(self, selector, text):
        self.wait_element(selector)
        el = self.find_element(selector)
        el.clear()
        el.send_keys(text)


    # 清除文本内容
    def clear(self, selector):
        self.wait_element(selector)
        el = self.find_element(selector)
        el.clear()


    # 封装点击元素的动作
    def click(self, selector):
        self.wait_element(selector)
        el = self.find_element(selector)
        el.click()

    #右击元素
    def right_click(self, selector):
        self.wait_element(selector)
        el = self.find_element(selector)
        ActionChains(self.driver).context_click(el).perform()

    #js操作
    def js(self, script):
        """调用js操作"""
        self.driver.execute_script(script)


    #通过js滑动页面
    def js_slide(self, nums=1000):
        """通过js滑动页面"""
        self.js(f"var q=document.documentElement.scrollTop={nums}")




    #通过js滑动到指定元素
    def js_elements(self, css):
        """通过js滑动到指定元素"""
        js = "arguments[0].scrollIntoView();"
        self.driver.execute_script(js, self.find_element(css))




    # 通过TouchActions滑动页面
    def touch_action_scroll(self, xoffset, yoffset):
        """
        :param xoffset: X offset to scroll to.
        :param yoffset: Y offset to scroll to.
        通过TouchActions滑动页面
        有时间试验一下，引入的包对不对
        """
        TouchActions(self.driver).scroll(xoffset, yoffset).perform()

    # 获取打开的url标题
    def get_page_title(self):
        return self.driver.title

    def get_url_text(self):
        """获取当前url"""
        return self.driver.current_url

    def open(self, url):
        """打开URL"""
        self.driver.get(url)

    # 获取警示框，并得到提示框信息和关闭提示框
    def get_alert(self):
        el = self.driver.switch_to.alert  # 获取窗口弹窗的方法
        el.accept()  # 点击弹窗确认按钮


    # 切换窗口
    def switch_window(self):
        windows = self.driver.window_handles
        self.driver.switch_to.window(windows[-1])

    @staticmethod  # 静态方法：不强制要求传递参数，类可以不用实例化就能调用该方法
    def sleep(seconds):
        time.sleep(seconds)

    #鼠标悬停：鼠标指向元素
    def move_to_element(self, selector):
        """鼠标悬停：鼠标指向元素"""
        self.wait_element(selector)
        el = self.find_element(selector)
        ActionChains(self.driver).move_to_element(el).perform()


    #双击元素
    def double_click(self, selector):
        """双击元素"""
        self.wait_element(selector)
        el = self.find_element(selector)
        ActionChains(self.driver).double_click(el).perform()


    #把元素el_css拖拽至元素ta_css
    def drag_and_drop(self, el_css, ta_css):
        """把元素el_css拖拽至元素ta_css"""
        element = self.find_element(el_css)
        target = self.find_element(ta_css)
        ActionChains(self.driver).drag_and_drop(element, target).perform()

    #通过partial_link_text定位元素并点击
    def click_text(self, text):
        """通过partial_link_text定位元素并点击"""
        self.driver.find_element_by_partial_link_text(text).click()

    #刷新页面
    def f5(self):
        """刷新页面"""
        self.driver.refresh()

    #获取当前元素的文案
    def get_text(self, selector):
        """获取当前元素的文案"""
        self.wait_element(selector)
        el = self.find_element(selector)
        return el.text

    #获取元素
    def get_attribute(self, selector, attribute):
        """获取元素"""
        el = self.find_element(selector)
        return el.get_attribute(attribute)

    #切换到弹框，点击弹框的确定
    def accept_alert(self):
        """切换到弹框，点击弹框的确定"""
        self.driver.switch_to.alert.accept()

    #关闭弹框：类似于点击弹框的取消
    def dismiss_alert(self):
        """关闭弹框：类似于点击弹框的取消"""
        self.driver.switch_to.alert.dismiss()

    #进入表单
    def switch_to_frame(self, selector):
        """进入表单"""
        iframe_el = self.find_element(selector)
        self.driver.switch_to.frame(iframe_el)

    #跳出表单
    def switch_to_frame_out(self):
        """跳出表单"""
        self.driver.switch_to.default_content()


    # 模拟键盘点击
    def keys_click(self, selector):
        el = self.find_element(selector)
        el.send_keys(Keys.ENTER)


    # 滑动底部
    def roll_down(self):
        # h滑动到底部
        js = "window.scrollTo(0,document.body.scrollHeight)"
        self.driver.execute_script(js)


    # 判断元素是否存在
    def is_element_exist(self, selector):
        flag = True
        try:
            self.find_element(selector)
            return flag
        except:
            flag = False
            return flag


    #下拉框选择方法，适用于select标签类型的下拉框
    def select_option(self, selector, select_type, value):
        """
        :param selector: 定位元素
        :param select_type: 选择方式
        0：以value属性值来查找该option并选择
        1：以index属性值来查找匹配的元素并选择
        2：以text文本值来查找匹配的元素并选择
        :param value: 选择方式对应的值
        :return:
        """
        element = self.find_element(selector)
        if select_type == 0:
            Select(element).select_by_value(value)
        elif select_type == 1:
            Select(element).select_by_index(value)
        elif select_type == 2:
            Select(element).select_by_visible_text(value)
        else:
            raise NameError("type输入错误，请核对")




    # 超时刷新方法，进入可能超时的页面后调用
    def refresh_when_timeout(self, times=5, sleeptime=2):
        url = self.driver.current_url
        for i in range(times):
            r = requests.request("post",url=url)
            status_code = r.status_code
            if status_code == 200:
                self.driver.refresh()
                break
            else:
                self.sleep(sleeptime)
                continue

    # 滑动寻找元素并点击
    def click_element_by_scroll(self, selector):
        exist = False
        count = 0
        while exist:
            exist = self.is_element_exist(selector)
            if exist:
                break
            else:
                count = count + 1
                print(count)
                self.glide(1)
                if count == 10:
                    exist = False
        self.glide(0.3)
        self.click(selector)


if __name__ == '__main__':
    filePath = BASE_PATH + '/report/images/'  # 设置存放截图的路径
    nowtime = datetime.datetime.now().strftime('%Y%m%d%H%M%S')  # 格式化时间
    imageName = filePath + nowtime + '.png'  # 定义截图文件名称 为了区分，可以传其他参数，做名字拼接
    print(imageName)