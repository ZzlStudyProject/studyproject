# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:      公共工具类

import yaml
import os
from faker import Faker
from webui.config.pathConfig import UIPAGEPATH
from webui.config.pathConfig import TESTCASEPATH
from webui.config.pathConfig import DATAPATH
from webui.config.pathConfig import DBSQLPATH
from webui.config.pathConfig import APIPATH
from webui.config.pathConfig import ELEMENTPATH
from webui.config.pathConfig import BASE_PATH
from webui.config.pathConfig import TESTPARAMDATAPATH
from webui.config.projectConfig import PROJECTNAME
from webui.config.pathConfig import CASETMEPLATEPATH
import configparser
#todo 后续有需要再次追加公共方法,注意一定是公共的工具型

class CommonTools:
    env="test"

    #获取yml文件数据
    def get_yaml_data(self,yaml_file):
        """
        获取yml文件数据,把yml文件当成一个对象提取除来，然后再按照yml文件格式，提前数据，如：data["addTestCaseData"]["int"]
        :param yaml_file: yml文件路径
        :return:
        """
        with open(yaml_file,"r",encoding="UTF-8") as f:
            data=f.read()
        yaml_data=yaml.safe_load(data)
        return yaml_data


    #获取元素数据
    def get_test_data(self, path, father_path='data', file='element.yml'):
        """
        获取元素数据，多封装了一层，当用到数据时，参数化可以少写一点。
        前提是元素的yml文件名称必须按照规定，写成element.yml 不这样写，就得多写代码，去找是不是存在元素的文件，多写好多代码，出规则就行了
        :param path:元素所在的文件夹，按照层级取看，就是data下面的，项目文件夹名称
        :return:
        """
        try:
            yaml_file = os.path.join(BASE_PATH, father_path, path, file)
            data = self.get_yaml_data(yaml_file)
            return data
        except Exception as e:
            print(e)




    #第三方库，随机生成各种数据
    def faker_random(self):
        """
        使用Faker生产各种数据，文本国家等等，用到的时候百度或者官网或者下面的连接
        不够去追加字典数据
        zh_CN 中文 zh_TW 台湾文，也就是繁体字
        https://zhuanlan.zhihu.com/p/87203290
        :return:
        """

        fake=Faker("zh_CN")
        faker_random_data={}
        faker_random_data['personName']=fake.name()
        faker_random_data['address'] = fake.address()
        faker_random_data['phoneNumber'] = fake.phone_number()
        faker_random_data['ssn'] = fake.ssn(min_age=18, max_age=90)# 身份证
        return faker_random_data



    def get_all_files(self, path=None):
        """
        遍历路径下所有的文件
        :param path: 完整的路径
        :return:
        """
        files_dict = {}
        files_list=[]
        for current_path,son_path,files in os.walk(path):

            files_dict['currentpath']=current_path
            files_dict['files'] = files
            files_dict['sonPath'] = son_path
            if len(son_path)==0:
                [files_list.append(os.path.join(current_path, i)) for i in files]
            elif len(son_path)!=0:
                [files_list.append(os.path.join(current_path, son_path, i)) for i in files]
            files_dict['files'] = files_list
        return files_dict


    def del_files(self, files):
        """
        删除文件，支持批量删除，支持单个删除
        :param files:
        :return:
        """
        if type(files) == list:
            for i in files:
                os.remove(i)
        elif type(files) == str:
            os.remove(files)
            print("文件夹里文件已清理")

    def getini(self,fliename,section=None,option=None):
        """
        获取配置文件数据
        :param fliename: 文件路径
        :param section: 第一标题
        :param option: 标题下内容
        :return:
        """
        iniflie=configparser.ConfigParser()
        iniflie.read(filenames=fliename,encoding='UTF-8')
        if section==None:
            return iniflie.sections()
        elif section!=None and option==None:
            return iniflie.options(section)
        else:
            return iniflie.get(section,option)


    def addini(self,fliename,section,option=None,values=''):
        """
        配置文件增加内容
        1.支持只增加，第一标题
        2.支持增加，第一标题 和 key值
        3.支持，增加，第一标题，key，还有vlaue
        4.后续还有需要再扩展
        :param fliename:配置文件路径
        :param section: 第一标题，必填
        :param option: 标题下的key值
        :param values: 标题下的vlaue值
        :return:
        """
        iniflie=configparser.ConfigParser()
        iniflie.read(filenames=fliename,encoding='UTF-8')
        if section not in iniflie.sections() and option==None:
                iniflie.add_section(section)
                with open(fliename, "w", encoding="utf-8") as f:
                    iniflie.write(f)
        elif section in iniflie.sections() and option not in iniflie.options(section):
                iniflie.set(section,option,values)
                with open(fliename, "w", encoding="utf-8") as f:
                    iniflie.write(f)
        else:
            print("属性值已存在")


    def changeini(self,fliename,section,option=None,values=''):
        """
        修改配置文件内容
        :param fliename:  fliename:配置文件路径
        :param section: 第一标题，必填
        :param option: 标题下的key值
        :param values: 标题下的vlaue值
        :return:
        """
        iniflie=configparser.ConfigParser()
        iniflie.read(filenames=fliename,encoding="utf_8")
        if section in iniflie.sections() and option in iniflie.options(section):
            iniflie.set(section, option, values)
            with open(fliename, "w", encoding="utf-8") as f:
                iniflie.write(f)
        else:
            print("要修改属性值不存在")



    def content_replace(self,content:str,old,new):
        """
        替换内容
        :param content: 原内容
        :param old: 被替换内容
        :param new: 替换内容
        :return:
        """
        result=content.replace(old,new)
        return result



    def create_new_file_for_template(self,readfile=None,newfile=None,file_content=None):
        """
        读取内容，写入新文件
        :param readfile: 读取文件内容
        :param newfile: 写入的新文件
        :param file_content: 直接写入内容。与readfile互斥
        :return:
        """
        if file_content ==None:
            with open(readfile,"r",encoding="utf-8") as f:
                template_content=f.read()

            with open(newfile,"w",encoding="utf-8") as  f:
                f.write(template_content)
        else:
            with open(newfile, "w", encoding="utf-8") as  f:
                f.write(file_content)





    def create_project_and_case(self):
        """
        1.新项目生成对应文件夹和初始相关文件
        2.根据data里面的testparamter的模板配置生成，用例文件(用例内容需要自己填充，如需要后续可扩展完全配置类型。)
        :return:
        """

        for i in PROJECTNAME:
            if os.path.exists(os.path.join(DATAPATH, i)) == False:
                os.mkdir(os.path.join(DATAPATH, i))
                self.create_new_file_for_template(readfile=APIPATH,newfile=os.path.join(DATAPATH,i,"api.py"))
                self.create_new_file_for_template(readfile=DBSQLPATH, newfile=os.path.join(DATAPATH, i,"db_sql.py"))
                self.create_new_file_for_template(readfile=ELEMENTPATH, newfile=os.path.join(DATAPATH, i,"element.yml"))
                self.create_new_file_for_template(readfile=TESTPARAMDATAPATH, newfile=os.path.join(DATAPATH, i,"testparamData.yml"))

            if os.path.exists(os.path.join(UIPAGEPATH,i)) == False:
                os.mkdir(os.path.join(UIPAGEPATH,i))

            if os.path.exists(os.path.join(TESTCASEPATH,i)) == False:
                os.mkdir(os.path.join(TESTCASEPATH,i))

        data = self.get_yaml_data(os.path.join(DATAPATH,i,"testparamData.yml"))


        for key in data:
            use_template=data[key]["template_info"]["use_template"]
            test_py_name = data[key]["template_info"]["test_py_name"]
            calss_name = data[key]["template_info"]["calss_name"]
            method_name = data[key]["template_info"]["method_name"]
            project_name=data[key]["template_info"]["project_name"]
            if use_template in "yes":
                if os.path.exists(os.path.join(TESTCASEPATH, i, test_py_name)) == False:
                    with open(CASETMEPLATEPATH, "r", encoding="utf-8") as f:
                        template_content = f.read()

                    result=self.content_replace(template_content,"{calss_name}",calss_name)
                    result=self.content_replace(result,"{method_name}",method_name)
                    result = self.content_replace(result, "{project_name}", project_name)
                    self.create_new_file_for_template(newfile=os.path.join(TESTCASEPATH,i,test_py_name),file_content=result)









if __name__ == '__main__':
    CommonTools().create_project()



