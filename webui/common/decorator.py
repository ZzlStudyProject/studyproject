# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:     需要需要自己写装饰器的，装饰器管理文件

from webui.config.pathConfig import IMAGEPATH
import datetime


# ui自动化截图的装饰器。开始是用闭包实现截图。但是有点小问题。弃用了，改为二次开发hook函数实现
#问题1，闭包和pytest参数化装饰器，结合使用。参数化的数据传不进去闭包函数。单独使用可以
#问题2，装饰在setup是不好使的，setup才开始实例driver，而闭包是在setup前传入driver，这时还没有呢，所以无法截图setup
def errorScreen(func):
    def wrapper(self, *args, **kwargs):
        try:
            func(self,*args, **kwargs)
        except:
            nowtime = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
            imageName = IMAGEPATH + nowtime + '.png'
            self.driver.get_screenshot_as_file(imageName)
            raise

