import time

from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy #集成By
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions


class AppBrowserDriver:

    def startDriver(self):
        start_set={}
        start_set["platformName"]="Android"
        start_set["platformVersion"] = "8.1"
        start_set["deviceName"] = "zzl"
        start_set["noReset"] = "true"#不对应用进行初始化，要不啥数据都没了
        start_set["appPackage"] = "com.tencent.wework"
        start_set["appActivity"] ='com.tencent.wework.launch.LaunchSplashActivity'
        #start_set['chromedriverExecutable']= "xxxx/chromedriver"  #对应driver的路径
        #start_set['automationName']='UiAutomator2'  toast使用 uiautomator2 引擎定位的  只不过在配置里面，默认的就是这个不用写

        self.driver=webdriver.Remote("http://127.0.0.1:4723/wd/hub",start_set)
        self.driver.implicitly_wait(5)

        # self.driver.find_element_by_xpath("//*[@text='通讯录']").click()
        # time.sleep(3)
        # self.driver.swipe(x*0.5,y*0.5,x*0.5,y,2)

        # self.driver.find_element_by_android_uiautomator(
        #     'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text("添加成员").instance(0));').click()
        time.sleep(5)
        return self.driver

        # self.driver.quit()


        # time.sleep(5)
        # driver.find_element_by_link_text("添加成员").click()
        # driver.find_element_by_link_text("手动输入添加").click()
        # driver.find_element_by_id("com.tencent.wework:id/ays").clear()
        # driver.find_element_by_id("com.tencent.wework:id/ays").send_keys("张七")
        # driver.find_element_by_id("com.tencent.wework:id/f4m").clear()
        # el=driver.find_element_by_id("com.tencent.wework:id/f4m").send_keys("18711111111")
        # driver.find_element_by_link_text("保存").click()
        # #是否可见，会有两个，appium的和selenium 看来还是最好分成两个环境写，互不干扰
        # el.is_displayed()
        # #是否可用
        # el.is_enabled()
        # #是否选中
        # el.is_selected()
        # #获取属性值
        # el.get_attribute("text") #比如 这个 ，获取 text属性的值，看源码，或者百度，看看能获取哪些属性值
        # #修改属性值
        # el.set_value()
        # #获取坐标  获取的是左上角的 坐标 ，x  y  是个属性，不是方法
        # el.location
        # #获取元素的 ，宽高，获取屏幕的宽高，有另外一个方法
        # el.size()
        # #要传driver对象的
        # action1=TouchAction(self.driver)
        # #perform()是执行命令 .wait(200) 太快加等待， 不建议直接坐标，建议屏幕大小 百分比
        # action1.press(x=731,y=141).wait(200).move_to(x=731,y=100).release().perform()
        # width=driver.get_window_rect()["width"]
        # height=driver.get_window_rect()["height"]
        # action1.press(x=int(width/2), y=int(height/2)).wait(200).move_to(x=int(width/3), y=int(height/2)).release().perform()
        # locator=(MobileBy.ID,"zhangsan")
        # WebDriverWait(driver,10).until(expected_conditions.element_to_be_clickable())
        # #返回demo树结构，去看toast的属性
        # print(driver.page_source)
        # #toast必须使用xpath定位
        # #使用ui啊u痛骂体哦女i
        # driver.find_element(MobileBy.XPATH,"//*[@class='a']").text
        # driver.find_element(MobileBy.XPATH, "//*[@text='b']").text
        # driver.find_element_by_link_text().get_attribute()

       #webview技术切换上下名文
        # print(driver.contexts)
        # driver.switch_to.context(driver.contexts[-1])
        # time.sleep(3)


    def quit_browser(self):
        self.driver.quit()

if __name__ == '__main__':
    AppBrowserDriver().startDriver()
