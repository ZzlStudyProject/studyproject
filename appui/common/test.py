# -*- coding: utf-8 -*-
# @CreateTime    : 2021/9/27
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/9/27 11:36
# @UpdateAuthor  : zhangzhenglai
# @Function:

#controller层，提供给前端的接口
def Login(username,password):
    #调用service层
   result = login_service(username,password)
   return result


#service层
def login_service(username,password):

    #调用dao层拿数据
    password_1=login_dao_password(username)
    if password == password_1:
        print("密码正确")

    return "处理结果，可能是一个页面，也可能是一堆参数"

#dao层
def login_dao_password(username):
    #调用jdbc可能还结合实体，去和数据库交互，拿出结果
    #再依次返出去
    pasword=entiy_jdbc_sql(username)
    return pasword

#jdbc层
def entiy_jdbc_sql():
    return 111
