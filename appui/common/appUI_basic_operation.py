from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.extensions.android.gsm import GsmCallActions
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
import time
from config.pathConfig import BASE_PATH

class AppBasicOperation:
    def __init__(self,driver=None):
        if driver==None:
            pass
        else:
            self.driver=driver

    # 封装定位方式，使用者只需要按照格式传参
    def find_element(self, location):
        """

        :param location: 定位方式拼接，例： xpath=>html/table
        :return: element  把定位元素返回，用于后续操作
        """
        try:
            bytype = location.split("=>")[0]
            value = location.split("=>")[1]
            print(bytype)
            print(value)
            if bytype == "xpath":
                element = self.driver.find_element_by_xpath(value)
            elif bytype == "css":
                element = self.driver.find_element_by_css_selector(value)
            elif bytype == "id":
                element = self.driver.find_element_by_id(value)
            elif bytype == "name":
                element = self.driver.find_element_by_name(value)
            elif bytype == "class_name":
                element = self.driver.find_element_by_class_name(value)
            # elif bytype == "link_text":   appium根本不支持，你改了底层也没用
            #     element = self.driver.find_element_by_link_text(value)
            elif bytype == "tag_name":
                element = self.driver.find_element_by_tag_name(value)
            elif bytype == "partial_link_text":
                element = self.driver.find_element_by_partial_link_text(value)
        except Exception as e:
            print(e)
        return element

    def find_elements(self, location):
        """

        :param location: 定位方式拼接，例： xpath=>html/table
        :return: element  把定位元素返回，用于后续操作
        """
        try:
            bytype = location.split("=>")[0]
            value = location.split("=>")[1]
            if bytype == "xpath":
                element = self.driver.find_elements_by_xpath(value)
            elif bytype == "css":
                element = self.driver.find_elements_by_css_selector(value)
            elif bytype == "id":
                element = self.driver.find_element_by_id(value)
            elif bytype == "name":
                element = self.driver.find_element_by_name(value)
            elif bytype == "class_name":
                element = self.driver.find_element_by_class_name(value)
            elif bytype == "link_text":
                element = self.driver.find_element_by_link_text(value)
            elif bytype == "tag_name":
                element = self.driver.find_element_by_tag_name(value)
            elif bytype == "partial_link_text":
                element = self.driver.find_element_by_partial_link_text(value)
        except Exception as e:
            print("请检查定位方式是否输入正确")
        return element


    #appdriver的方法只会更多，uiautomator2等 比如，触屏的操作，滚动，等等，等等，都扩展在这里

    def clicktest(self):
        el=self.driver.find_element_by_xpath("//*[@text='通讯录']")
        el.click()
    # 封装点击元素的动作
    def click(self, selector):
        # self.element_wait(selector)
        el = self.find_element(selector)
        el.click()

    def el_get_text(self,el):
        return el.text

    # 封装输入框方法
    def type(self, selector, text):
        # self.element_wait(selector)
        el = self.find_element(selector)
        el.clear()
        el.send_keys(text)

    def back(self):
        self.driver.back()

    # 判断元素是否存在
    def isElementExist(self, selector):
        flag = True
        try:
            self.find_element(selector)
            return flag
        except:
            flag = False
            return flag

    #滑动
    def Slides(self):
        TouchAction


    #滚动查找
    def swiper_find(self,selector):
        num=10
        x = self.driver.get_window_rect()["width"]
        y = self.driver.get_window_rect()["height"]
        print(x)
        print(y)
        for i in range(0,num):

            if i==num-1:
                return f"向上滑动了{num}次没找到"
            try:
                return self.find_element(selector)
            except Exception as e:
                print("没找到元素需要滑动")
                print(e)
                start_x=x/2
                start_y=y*0.8
                end_x=x/2
                end_y=y*0.3
                findtime=2000 #ms 2秒
                self.driver.swipe(start_x,start_y,end_x,end_y,findtime)
        # TouchAction(self.driver).press(x=int(width/2), y=int(height/2)).wait(200).move_to(x=int(width/2), y=int(height)).release().perform()
        # #perform()是执行命令 .wait(200) 太快加等待

    #使用uiautomator固定到你要定位的地方并点击，现在只传text扩展其他的定位方式。而且这种定位方式，太死板了，没有上面自己写的灵活
    def rollToElementClick(self,text):
        self.driver.find_element_by_android_uiautomator(
            f'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text("{text}").instance(0));').click()

    #模拟来电话
    def  givePhone(self):
        self.driver.make_gsm_call("18210466329",GsmCallActions.CALL)

    # 模拟来短信
    def sendSMS(self):
        self.driver.send_sms("18210466329", "Hey lol");

    #模拟网络情况
    def network(self):
        #看appium的源码，二进制代表啥网络
        self.driver.set_network_connection(4)

    #截取图片
    def get_window_img(self, fun_name):
        file_path = BASE_PATH + '/report/images/'  # 设置存放截图的路径
        # print('截图保存路径为：%s' % file_path)
        timeset = time.strftime('%m%d%H%M%S', time.localtime(time.time()))  # 格式化时间
        pic_name = file_path + fun_name + timeset + '.png'  # 定义截图文件名称
        try:
            self.driver.get_screenshot_as_file(pic_name)
        except Exception as e:
            self.get_window_img('截图异常')


    #录屏，停止录屏，要看支持版本和，手机型号（比如 华为不行）