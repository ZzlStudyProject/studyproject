import yaml
import os

from faker import Faker

from config.pathConfig import BASE_PATH
import random


class CommonTools:
    env="test"

    def get_yaml_data(self,yaml_file):
        with open(yaml_file,"r",encoding="UTF-8") as f:
            data=f.read()
        yaml_data=yaml.safe_load(data)
        return yaml_data


    def get_element_data(self,path):
        try:
            yaml_file = os.path.join(BASE_PATH, 'data', path, 'element.yml')
            data = self.get_yaml_data(yaml_file)
            return data
        except Exception as e:
            print(e)


    #给pytest用的
    def select_data_for_env(self,path):
        try:
            if self.env=="test":
                yaml_file = os.path.join(BASE_PATH, 'data',path, 'testparamData.yml')
            elif self.env=="dev":
                yaml_file = os.path.join(BASE_PATH, 'data', path, 'devparamData.yml')
            elif self.env=="prd":
                yaml_file = os.path.join(BASE_PATH, 'data', path, 'prdparamData.yml')
            data = self.get_yaml_data(yaml_file)
            return data
        except Exception as e:
            print("环境信息输入有误，未找到环境信息")

    def phone(self):
        prelist = ["130", "131", "132", "133", "134", "135", "136", "137", "138", "139", "147", "150", "151", "152",
                   "153",
                   "155", "156", "157", "158", "159", "186", "187", "188"]
        return random.choice(prelist) + "".join(random.choice("0123456789") for i in range(8))


    def randomName(self):
        headnamelist=["钱","孙","李","周","吴","郑","王","冯","陈","褚","卫","蒋","沈","韩","杨","朱","秦","尤","许"]
        tailnamelist=["秀","娟","英","华","慧","巧","美","娜","静","淑","惠","珠","翠","雅","芝","玉","萍","红","伟","刚","勇","毅","俊"]
        return random.choice(headnamelist)+"".join(random.choice(tailnamelist) for i in range(2))



    def fakername(self):
        #使用Faker生产各种数据，文本国家等等，用到的时候百度或者官网或者下面的连接
        #https://zhuanlan.zhihu.com/p/87203290
        fake=Faker("zh_CN") #zh_CN 中文 zh_TW 台湾文，也就是繁体字
        print(fake.name())
        print(fake.address())
        print(fake.phone_number())
        fake.ssn(min_age=18, max_age=90)    # 身份证


#这个就是调用fixture但是不知道怎么用它区分。最后区分环境数据，还是用了自己写的方法
# def test_getENV(get_env):
#     return get_env


if __name__ == '__main__':
    CommonTools().fakername()


