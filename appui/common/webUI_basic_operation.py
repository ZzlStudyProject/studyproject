import time
from telnetlib import EC

from selenium import webdriver
import yaml

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

#把selenium基本操作，做封装，而且也相当于方法库，直接提供给他人写代码时，直接调用。发现方法不足时，再丰富追加，全局的谁都可以调用



class BasicOperation:
    #driver实例化抽离出来
    def __init__(self,base_driver:webdriver):
        #其他知识点，init函数是不可以return的
        #为什么判断？
        # 因为每个page类都继承基类，那只要加载每个page对象，都会走基类的init方法，都会重新实例化一个driver
        #在每个page方法里面的return时，直接把driver，传递下去 如： return AddMemberPage(self.driver)
        #解决，driver多次实例化，多次启动窗口问题
        if base_driver == None:
            otp = webdriver.ChromeOptions()
            otp.debugger_address = "127.0.0.1:9222"
            self.driver = webdriver.Chrome(options=otp)
            self.driver.implicitly_wait(10)

            # 存cookie
            cookies = self.driver.get_cookies()
            with open(r"E:\沃格霍兹学院\studyproject\UIpage\workWechat\cookies.yml", "w", encoding="utf-8") as f:
                yaml.dump(cookies, f)


            # self.driver = webdriver.Chrome()
            # 取cookie
            with open(r"E:\沃格霍兹学院\studyproject\UIpage\workWechat\cookies.yml", "r", encoding="utf-8") as f:
                # 最好有safe_load 最新的，load可能以后不支持了
                cookies = yaml.safe_load(f)
            # 加cookie,想加cookie必须先打开个url
            self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
            self.driver.maximize_window()
            for cookie in cookies:
                self.driver.add_cookie(cookie)
            self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
            time.sleep(10)


        else:
            self.driver=base_driver
            # with open(r"E:\沃格霍兹学院\studyproject\UIpage\workWechat\cookies.yml", "r", encoding="utf-8") as f:
            #     # 最好有safe_load 最新的，load可能以后不支持了
            #     cookies = yaml.safe_load(f)
            # for cookie in cookies:
            #     self.driver.add_cookie(cookie)


    #封装定位方式，使用者只需要按照格式传参
    def find_element(self,location):
        """

        :param location: 定位方式拼接，例： xpath=>html/table
        :return: element  把定位元素返回，用于后续操作
        """
        try:
            bytype=location.split("=>")[0]
            value=location.split("=>")[1]
            if bytype == "xpath":
                element = self.driver.find_element_by_xpath(value)
            elif bytype == "css":
                element = self.driver.find_element_by_css_selector(value)
            elif bytype == "id":
                element=self.driver.find_element_by_id(value)
            elif bytype == "name":
                element = self.driver.find_element_by_name(value)
            elif bytype == "class_name":
                element=self.driver.find_element_by_class_name(value)
            elif bytype =="link_text":
                element=self.driver.find_element_by_link_text(value)
            elif bytype == "tag_name":
                element=self.driver.find_element_by_tag_name(value)
            elif bytype == "partial_link_text":
                element=self.driver.find_element_by_partial_link_text(value)
        except Exception as e:
            print(e)
        return element


    def find_elements(self,location):
        """

        :param location: 定位方式拼接，例： xpath=>html/table
        :return: element  把定位元素返回，用于后续操作
        """
        try:
            bytype=location.split("=>")[0]
            value=location.split("=>")[1]
            if bytype == "xpath":
                element = self.driver.find_elements_by_xpath(value)
            elif bytype == "css":
                element = self.driver.find_elements_by_css_selector(value)
            elif bytype == "id":
                element=self.driver.find_element_by_id(value)
            elif bytype == "name":
                element = self.driver.find_element_by_name(value)
            elif bytype == "class_name":
                element=self.driver.find_element_by_class_name(value)
            elif bytype =="link_text":
                element=self.driver.find_element_by_link_text(value)
            elif bytype == "tag_name":
                element=self.driver.find_element_by_tag_name(value)
            elif bytype == "partial_link_text":
                element=self.driver.find_element_by_partial_link_text(value)
        except Exception as e:
            print("请检查定位方式是否输入正确")
        return element


    def click(self, selector):
        # self.element_wait(selector)
        el = self.find_element(selector)
        el.click()


    # 封装输入框方法
    def type(self, selector, text):
        # self.element_wait(selector)
        el = self.find_element(selector)
        el.clear()
        el.send_keys(text)


    #各种封装公用方法都往里面补充，

    # def element_wait(self, selector, secs=5):
    #     """等待元素时长"""
    #     if "=>" not in selector:
    #         raise NameError("Positioning syntax errors, lack of '=>'.")
    #
    #     by = selector.split("=>")[0]
    #     value = selector.split("=>")[1]
    #
    #     if by == "id":
    #         WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.ID, value)))
    #     elif by == "name":
    #         WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.NAME, value)))
    #     elif by == "class_name":
    #         WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.CLASS_NAME, value)))
    #     elif by == "link_text":
    #         WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.LINK_TEXT, value)))
    #     elif by == "partial_link_text":       #jiangchunliang
    #         WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, value)))  #jiangchunliang
    #     elif by == "xpath":
    #         WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.XPATH, value)))
    #     elif by == "css":
    #         WebDriverWait(self.driver, secs, 1).until(EC.presence_of_element_located((By.CSS_SELECTOR, value)))
    #     else:
    #         raise NameError(
    #             "Please enter the correct targeting elements,'id','name','class','link_text','xpath','css'.")

    # 封装点击元素的动作
