from common.webUI_basic_operation import BasicOperation

class CommonPage(BasicOperation):
    """
    针对整个项目的公共页面复用操作，比如：每个项目组有自己负责的模块，但这些模块的页面流程
    又都需要通过登录，或者导航栏，翻页等等。这些功能不能由各业务自己去写，不统一也不是分属各个业务组的
    拿出来，做个同一的，由负责人统一维护
    一般这种情况有，不是很多。所有业务模块都会调用的，return的地方基本还得统一
    也就几个登录等。。获取cookie，甚至复用率不高，完全没有必要，拿到common层
    """


    def UIlogin(self):
        return

    def getCookie(self):

        return