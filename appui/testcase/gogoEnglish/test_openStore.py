import os
import pytest
from config.pathConfig import BASE_PATH
from project.gogoEnglish.buyCourse import BuyCourse
import allure
from common.commonTools import CommonTools
from fixture.gogoEnglish.gogoEnglishfixture import testData_for_openStore
from fixture.gogoEnglish.gogoEnglishfixture import testYamlData_for_openStore
import time
data=CommonTools().select_data_for_env("gogoEnglish")

#分类，是为了，让报告看起来更加的清晰，feature大类
@allure.feature("打开商城功能")
class TestOpenStore:


    #story小类别
    @allure.story("打开商城接口1")
    @pytest.mark.run(order=2)
    #引用非conftest.py文件的fixture函数，要导入
    def test_openStore_01(self,testData_for_openStore):
        #返回的是什么格式，就用什么格式接受，比如这个是list的格式。返回{}格式久用key value
        #可能多返参数，只取你想要的
       result=BuyCourse().openStore(testData_for_openStore[0],testData_for_openStore[1],testData_for_openStore[2])
       time.sleep(10)
       assert result==testData_for_openStore[3]

    @allure.story("打开商城接口2")
    @pytest.mark.run(order=1)
    def test_openStore_02(self,testYamlData_for_openStore):
        result = BuyCourse().openStore(testYamlData_for_openStore[0], testYamlData_for_openStore[1], testYamlData_for_openStore[2])
        assert result == testYamlData_for_openStore[3]



    # def test_openStore_02(self,a,b,c,expectedValue):
    #     result = BuyCourse().openStore(a, b, c)
    #     assert result == expectedValue


if __name__ == '__main__':
    path1=os.path.join(BASE_PATH,"testcase")
    path2=os.path.join(BASE_PATH,"report")
    #执行用例，并把用例生成报告，path2是报告文件
    pytest.main(["-v","-s",path1,"--alluredir",path2])
    #allure serve ./report  ,命令行，生产报告，其实是启动要给报告服务
    cmd="allure serve "+path2
    os.system(cmd)


