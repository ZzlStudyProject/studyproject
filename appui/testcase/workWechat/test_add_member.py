import allure

from UIpage.workWechat.homePage import HomePage
import pytest
from common.commonTools import CommonTools
import os
from config.pathConfig import BASE_PATH
from common.commonTools import CommonTools

from common.browser_engine import BrowserDriver

@allure.feature("web端ui自动化")
class TestAddMember:

    def setup_method(self):
        self.driver=BrowserDriver().openbrwose(url="https://work.weixin.qq.com/wework_admin/frame#index")



    def teardown_method(self):
        self.driver.close()
        print("结束了")

    @allure.story("添加成员")
    @pytest.mark.parametrize("assertname,name,jiancheng,bianhao,phone",[["正来来来来","正来来来来","正来第三","003","13911110004"]])
    def test_add_member(self,assertname,name,jiancheng,bianhao,phone):
        #         1.主页点击去通讯录页，2.通讯录页取添加成员页，3添加成员页添加成员，4.通讯录页返回成员列表
        names=HomePage(self.driver).go_to_contactsPage().go_to_addMemberPage().add_member(name,jiancheng,bianhao,phone).get_contact_list()
        print(names)
        assert names[0] == assertname

