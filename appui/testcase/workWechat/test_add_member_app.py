import allure
import pytest

from UIpageApp.workWechatApp.messagePage import MessagePage
from common.appbrowser_engine import AppBrowserDriver
from common.commonTools import CommonTools


@allure.feature("app端自动化")
class TestAddMemberApp:

    def setup_method(self):
        self.driver=AppBrowserDriver().startDriver()

    def teardown_method(self):
        self.driver.quit()
        print("结束运行")


    @allure.story("添加成员")
    @pytest.mark.parametrize("addname", [(CommonTools().random_name())])
    def test_add_member_app(self,addname):
        """
        1.消息页点击通讯录
        2.通讯录点击添加成员去添加页
        3.添加页点击手动添加去手动添加页
        4.手动添加页，添加成员保存，返回手动添加页
        5.手动添加页返回通讯页，获取成员数据
        6.断言刚才添加的人是否添加成功
        :return:
        """
        namelist = MessagePage(self.driver).go_to_phonebookPage().go_to_addmemberPage().go_to_personaddPage().addMember(addname).back_to_phonebookPage().get_member()
        assert addname in namelist


