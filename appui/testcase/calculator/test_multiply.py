import os
from config.pathConfig import BASE_PATH

from project.calculator.calculator import Calculator
import pytest
from common.commonTools import CommonTools
data=CommonTools().select_data_for_env("calculator")



class TestMultiply:



    #@pytest.mark.skip(reason="这条跳过不执行")
    # @pytest.mark.skip(ondition="1==1",reason="当满足前面的条件时,这条跳过不执行")
    @pytest.mark.parametrize("a,b,expectedValue", data["multiplyTestCaseData"]["int"])
    #使用fixture类前后置
    def test_multiply_int(self,setupAndteardown_class_fixture,a,b,expectedValue):
        result=Calculator().multiply(a,b)
        assert result == expectedValue

    @pytest.mark.parametrize("a,b,expectedValue", data["multiplyTestCaseData"]["float"])
    def test_multiply_float(self,a,b,expectedValue):
        result=Calculator().multiply(a,b)
        assert result == expectedValue



if __name__ == '__main__':
    path = os.path.join(BASE_PATH, 'testcase')
    path1 = os.path.join(BASE_PATH, 'testcase', "calculator", "test_multiply.py")
    # 运行目录下的用例，那级用例，你自己定
    pytest.main(["-v", "-s", path1])