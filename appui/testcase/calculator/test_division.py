import os
from config.pathConfig import BASE_PATH
from project.calculator.calculator import Calculator
import pytest
import allure
from common.commonTools import CommonTools
data=CommonTools().select_data_for_env("calculator")


@allure.feature("除法的事件")
class TestDivison:



    @allure.step("第一步先测试int类型，（备注测试步骤的）")
    # @allure.attach("具体的信息，可文本，可视频，可图片")
    @pytest.mark.run(order=2)
    # 失败后重试3次，每次间隔1秒，这个间隔参数也可以不要
    @pytest.mark.flaky(reruns=6, reruns_delay=1)
    @pytest.mark.parametrize("a,b,expectedValue", data["divisionTestCaseData"]["int"])
    #调用fixture方法scope="module"模块级别，然后使用后命令行去执行，这个py文件，pytest.main(["-v", "-s", path1])
    #看日志，你就会发现，用例执行前，调用一次module_fixture，执行的是yeild前的代码。但是所有用例执行后也调用一次。执行的
    #是yeild的d代码。这个所有用例执行后。又执行了一次。不是fixture的功能，是yeild的功能。你直接写return，它也是只执行一次。
    #有时会用，yeild，是结合使用，省得再写一个 相当于setup和teardown结合了。
    #你是在第一条用例调用的。如果你不在第一条用例调用。比如把第二条order=1先主执行
    # 你就会发现还是等启用这一条的时候，才加载。也就是说，就是一个方法，那条用例调。那条前加载。
    # 即使你是module的范围。也没用。如果你是第一条赶巧了用了。没问题，不是第一条，也可能达不到你想要的效果，比如你想整个文件前
    # 先执行，不管那个用例。再一个注意点，这个是给命令行用的。你使用pytst运行，不认识的，不会走yeild
    def test_division_int(self,module_fixture, a, b, expectedValue):
        result = Calculator().division(a, b)
        assert result == expectedValue

    @allure.story("float类型测试点")
    @pytest.mark.run(order=1)
    @pytest.mark.parametrize("a,b,expectedValue", data["divisionTestCaseData"]["float"])
    def test_division_float(self, a, b, expectedValue):
        result = Calculator().division(a, b)
        assert result == expectedValue

    @pytest.mark.parametrize("a,b,expectedValue", data["divisionTestCaseData"]["string"])
    def test_division_string(self, a, b, expectedValue):
        result = Calculator().division(a, b)
        assert result == expectedValue

    @pytest.mark.parametrize("a,b,expectedValue", data["divisionTestCaseData"]["none"])
    def test_division_none(self, a, b, expectedValue):
        result = Calculator().division(a, b)
        assert result == expectedValue


if __name__ == '__main__':
    path = os.path.join(BASE_PATH, 'testcase')
    path1 = os.path.join(BASE_PATH, 'testcase', "calculator", "test_division.py")
    # 运行目录下的用例，那级用例，你自己定
    pytest.main(["-n","2",path1])