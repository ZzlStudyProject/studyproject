
import pytest
import os
from project.calculator.calculator import Calculator
from config.pathConfig import BASE_PATH
from common.commonTools import CommonTools

data=CommonTools().select_data_for_env("calculator")


class TestSubtraction:

    #这种是不接收返回值的调用方式
    @pytest.mark.usefixtures("setupAndteardown_class_fixture")
    # @pytest.mark.jianfa
    #ids作用就是给用例起标题的，中文需要转码,没解决掉
    @pytest.mark.parametrize("a,b,expectedValue", data["subtractionTestCaseData"]["int"],
                             ids=["负数","正数","小数","非数字类型"])
    def test_subtraction_int(self,a,b,expectedValue):
        result=Calculator().subtraction(a,b)
        assert result == expectedValue

if __name__ == '__main__':
    path = os.path.join(BASE_PATH, 'testcase')
    path1 = os.path.join(BASE_PATH, 'testcase', "gogoEnglish")
    # 运行目录下的用例，那级用例，你自己定
    pytest.main(["-v", "-s", path1])