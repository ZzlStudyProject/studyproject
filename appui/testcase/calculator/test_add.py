import os
from config.pathConfig import BASE_PATH
from project.calculator.calculator import Calculator
import pytest
import allure
from common.commonTools import CommonTools
data=CommonTools().select_data_for_env("calculator")

#每个方法，可能用例类型，就不同，比如 int一个类型，float一个类型，而一个类型，就可能有多组用例。
#所以这样分组更好一些，这样写，那么就要对，yaml文件的分组下功夫了。
@allure.feature("计算器加法功能")
class TestAdd:

    @classmethod
    def setup_class(self):
        print("类前置")

    @classmethod
    def teardown_class(self):
        print("类后置")

    def setup_method(self):
        print("方法前置")

    def teardown_method(self):
        print("方法后置")


    @pytest.mark.run(order=1)
    # @pytest.mark.jiafa
    #两种参数化的方式，一个合起来，合着的后面格式[(1,2,4),(1,5,7)]或者[[1,2,4],[1,5,7]]
    # 一个是分着的,分着的格式如下。分着是排列组合了
    #@pytest.mark.parametrize("a,b,expectedValue",data["addTestCaseData"])
    @pytest.mark.parametrize("a", [1,2,6])
    @pytest.mark.parametrize("b", [2,4])
    @pytest.mark.parametrize("expectedValue", [3, 6])
    def test_add(self,a,b,expectedValue):
        result=Calculator().add(a,b)
        assert result == expectedValue


    @allure.story("int数据类型测试")
    @pytest.mark.parametrize("a,b,expectedValue",data["addTestCaseData"]["int"])
    def test_add_int(
            self,a,b,expectedValue):
        with allure.step("测试步骤"):
            print("测试步骤是写在方法里面的,可写多个步骤，,with allure.step('测试步骤')"':')
        result=Calculator().add(a,b)
        assert result == expectedValue

    @allure.story("float数据类型测试")
    @pytest.mark.parametrize("a,b,expectedValue",data["addTestCaseData"]["float"])
    def test_add_float(self,a,b,expectedValue):
        #round 0.1+0.2=0.300004 是二进制加法，加出来不是0.3  使用round函数
        #round函数，四舍五入，第二个参数是，保留几位小数
        result=round(Calculator().add(a,b),2)
        assert result == expectedValue

    @allure.story("string数据类型测试")
    @pytest.mark.parametrize("a,b,expectedValue",data["addTestCaseData"]["string"])
    def test_add_string(self,a,b,expectedValue):
        result=Calculator().add(a,b)
        assert result == expectedValue

    @allure.story("none数据类型测试")
    @pytest.mark.parametrize("a,b,expectedValue",data["addTestCaseData"]["none"])
    def test_add_none(self,a,b,expectedValue):
        result=Calculator().add(a,b)
        assert result == expectedValue









if __name__ == '__main__':
    #第一种运行方式,全部运行
    # pytest.main()
    #加参数运行，-k匹配，后面是匹配字符,-s显示打印，-v详细，-x遇到失败停止
    #-m 后面是运行有啥标签的用例,--maxfail=2 最大失败数停止
    # pytest.main(["-s","-v","-k","add","-x","-m","jiafa","--maxfail=2"])
    path = os.path.join(BASE_PATH, 'testcase')
    path1 = os.path.join(BASE_PATH, 'testcase',"calculator","test_add.py")
    # 运行目录下的用例，那级用例，你自己定
    pytest.main(["-v","-s","--env=dev",path1])
    #运行类， 运行类里面的指定用例。就看你 ：： 到哪里
    #pytest.main([path1+"::TestCalculator::test_division"])




