import pytest
import os
from common.commonTools import CommonTools
from config.pathConfig import BASE_PATH


data=CommonTools().select_data_for_env("gogoEnglish")



@pytest.fixture()
def login():
    username="zhangsan"
    password="1122"
    loginStatus=""
    accountMoney=""

    if(username=="zhangsan" and password=="1122"):
        loginStatus="登录成功"
        accountMoney="账户余额充足"
    else:
        loginStatus="登录失败"
        accountMoney="账户余额无法获取"
    return loginStatus,accountMoney


#这个就是使用fixture参数化。那个方法调用这个fixture函数就把这个给他们。
@pytest.fixture(params=[[1,2,4,"打开商城成功"],[1,2,7,"打开商城成功"],[1,6,4,"打开商城成功"]],
                ids=["第1条测试","第2条测试","第3条测试"])
def testData_for_openStore(request):
    return request.param


#fixture参数化一样可以结合，yaml使用而且肯定都会数据分离的。
@pytest.fixture(params=data["openStore"]["TestCaseData"],ids=data["openStore"]["ids"])
def testYamlData_for_openStore(request):
    return request.param


if __name__ == '__main__':
    print(data["openStore"]["TestCaseData"])
    print(data["openStore"]["ids"])

