import pymysql



class DateBase:
    """
    数据库连接的基类，直接把数据库参数写在这里，包装查询方法
    后续其他人写自己业务时，直接集成调用
    """



    def __init__(self,env=None):
        """
        #根据环境选择库，当然库多，跨库，增加标识.库参数直接写死，因为就是config文件
        :param env:
        """
        try:
            db_config_defult = {
                "host": "127.0.0.1",
                "port": 3306,
                "user": "root",
                "passwd": "zhangzhenglai"
            }
            db_config_dev={
                "host": "127.0.0.1",
                "port": 3306,
                "user": "root",
                "passwd": "zhangzhenglai"
            }
            db_config_test = {
                "host": "127.0.0.1",
                "port": 3306,
                "user": "root",
                "passwd": "zhangzhenglai"
            }
            if env=="dev":
                coon = pymysql.connect(**db_config_dev)
                #self.cur=xxx,就是你在class里面定义一个 cur=“” 然后在init初始化这个类属性，也可以直接这样写
                self.cur = coon.cursor(pymysql.cursors.DictCursor)
            elif env=="test":
                coon = pymysql.connect(**db_config_test)
                # 创建游标,参数pymysql.cursors.DictCursor是返回类型是字典，就是把字段名是key，值是value
                self.cur = coon.cursor(pymysql.cursors.DictCursor)
            elif env==None:
                coon = pymysql.connect(**db_config_defult)
                #self.cur是类属性，赋值，要写到循环里面，写在外面，是不行的，因为coon变量的作用域是在条件语句里面的，出去了不认识了
                #想放在外面，也可以，那就在try的作用域，定义一个coon，或者在最外层类方法里面定义一个，比较丑就不这么写了
                self.cur = coon.cursor(pymysql.cursors.DictCursor)
        except Exception as e:
            print("数据库连接出问题了",str(e))

    def query(self,sql):
        """
        查询语句，返回结果
        :param sql:
        :return:
        """
        try:
            self.cur.execute(sql)
            return self.cur.fetchall() #返回执行结果
        except Exception as e:
            print('sql语句有误！%s' % e)
        finally:
            self.cur.close()



    def table_exists(self,databaseName, tableName):
        sql = f"SELECT table_name FROM {databaseName}.TABLES WHERE table_name ='{tableName}';"
        if self.query(sql):
            return True










if __name__ == '__main__':
    print()

