"""HTTP-specific events."""

#请求时，就直接创建一个reponse对象，而不是象刚才，截取返回修改返回，再塞回去，这回是直接就不请求服务端了，截取了直接就返回
#你也可以，用刚才的方式，在reposne去写，那就不截取了，直接写个返回值，返给reponse就行了
import json



from mitmproxy import http
class events:


    def request(self,flow: http.HTTPFlow):
        # # redirect to different host
        # if flow.request.pretty_host == "example.com":
        #     flow.request.host = "mitmproxy.org"
        # # answer from proxy
        # elif flow.request.path.endswith("/brew"):
        #     flow.response = http.HTTPResponse.make(
        #         418, b"I'm a teapot",
        #     )
        if "A"  in flow.request.url:
            print("这是v3接口的请求数据",flow.request.text)
            with open("resultdata.json","r",encoding="utf-8") as f:
                data=f.read()
            str(data)
            print(str(data))
            flow.response = http.HTTPResponse.make(
                    #响应头
                    200,
                    #body
                    str(data)
            )

addons = [
    events()
]

if __name__ == '__main__':
    from mitmproxy.tools.main import mitmdump
    #启动debug模式，__file__是当前文件
    mitmdump(["-p","8080","-s",__file__])