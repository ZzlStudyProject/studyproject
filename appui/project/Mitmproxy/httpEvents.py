"""HTTP-specific events."""

#http 请求的经历的过程，做定制就是在这里
import json

import mitmproxy.http


class Events:

    #mitmproxy的event严格按照固定名称命名，其实就是 hook函数，  这是request，下面是reponse，还有其他，先屏蔽了。还有其他的
    #当然，不是http协议，还有其他协议的，到时候可以官网去copy再重写  https://docs.mitmproxy.org/stable/addons-events/
    def request(self, flow: mitmproxy.http.HTTPFlow):
        """
            The full HTTP request has been read.
        """
        #print("这是flow.request的内容：",flow.request)
        # print("这是flow.request.url的内容：",flow.request.url)
        #print("还有flow.request.其他内容：",flow.request.data,flow.request.text,flow.request.cookies)
        #匹配一下
        if "a" in flow.request.url:
            print("这是v3接口的请求数据",flow.request.text)

    def response(self, flow: mitmproxy.http.HTTPFlow):
        """
            The full HTTP response has been read.
        """
        if "a" in flow.request.url:
            print("这是v3接口的响应数据",flow.response.text)
            print("看下格式",type(flow.response.text))
            print("转化一下格式",json.loads(flow.response.text))
            data=json.loads(flow.response.text)
            print(type(data))
            print(data["result"]["cn"][0])  #打印一个
            print(len(data["result"]["cn"]))  #看下列表的长度
            print(type(data["result"]["cn"]))  #看下类型是否是list
            data["result"]["cn"].append(data["result"]["cn"][0])  #类型是list就用append方法追加元素
            data["result"]["cn"].append(data["result"]["cn"][1])  #如果类型是dict就用字典的 key =value形式追加
            print(len(data["result"]["cn"]))

            print("上面是把数据修改了，把cn数据由6个变为了8个，下面就把数据塞回给reposne 给返出去")
            print("追加的没成功，改成了把cn由6个改成了2个，测试成功，最主要一句就是   flow.response.text= 赋值，上，注意 格式就行了")
            print("")
            data = {"a":"数据防止泄露"}

            flow.response.text= str(data)




    # def responseheaders(self, flow: mitmproxy.http.HTTPFlow):
    #     """
    #         HTTP response headers were successfully read. At this point, the body
    #         is empty.
    #     """
    #
    # def http_connect(self, flow: mitmproxy.http.HTTPFlow):
    #     """
    #         An HTTP CONNECT request was received. Setting a non 2xx response on
    #         the flow will return the response to the client abort the
    #         connection. CONNECT requests and responses do not generate the usual
    #         HTTP handler events. CONNECT requests are only valid in regular and
    #         upstream proxy modes.
    #     """
    #     print(flow.type)
    #     print("这是链接")
    #
    # def requestheaders(self, flow: mitmproxy.http.HTTPFlow):
    #     """
    #         HTTP request headers were successfully read. At this point, the body
    #         is empty.
    #     """
    #     print(flow.request)
    #     print("这是请求头")
    #
    # def error(self, flow: mitmproxy.http.HTTPFlow):
    #     """
    #         An HTTP error has occurred, e.g. invalid server responses, or
    #         interrupted connections. This is distinct from a valid server HTTP
    #         error response, which is simply a response with an HTTP error code.
    #
    #     """


from project.Mitmproxy.mitmproxyStudy import Counter
# addons = [
#     Events()
# ]

# if __name__ == '__main__':
#     from mitmproxy.tools.main import mitmdump
#     #启动debug模式，__file__是当前文件
#     mitmdump(["-p","8080","-s",__file__])