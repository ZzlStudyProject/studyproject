from common.appUI_basic_operation import AppBasicOperation
from common.commonTools import CommonTools
data=CommonTools().get_test_data("workWechat")


class PhoneBookPage(AppBasicOperation):
    addmember=data["phonebookPageApp"]["addmember"]
    names=data["phonebookPageApp"]["names"]

    #去添加成员页
    def go_to_addmemberPage(self):
        self.swiper_find(self.addmember).click()
        # self.rollToElementClick(self.addmember)
        #self.click(self.addmember)
        from UIpageApp.workWechatApp.addMemberPage import AddMemberPage
        return AddMemberPage(self.driver)


    #获取页面上的成员
    def get_member(self):
        namelist=[]
        names=self.find_elements(self.names)
        for el in names:
            namelist.append(self.el_get_text(el))

        print(namelist)
        return namelist