from common.appUI_basic_operation import AppBasicOperation
from common.commonTools import CommonTools

data=CommonTools().get_test_data("workWechat")

class PersonBookPage(AppBasicOperation):
    nameinput=data["personAddPageApp"]["nameinput"]
    phoneinput=data["personAddPageApp"]["phoneinput"]
    save=data["personAddPageApp"]["save"]
    inform=data["personAddPageApp"]["inform"]
    popup=data["personAddPageApp"]["popup"]
    confirm=data["personAddPageApp"]["confirm"]

    #添加功能
    def addMember(self,addname):
        phone = CommonTools().phone()
        self.click(self.inform)
        self.type(self.nameinput,addname)
        self.type(self.phoneinput,phone)
        self.click(self.save)
        print(self.isElementExist(self.popup))

        for n in range(0,10):
            phone = CommonTools().phone()
            if self.isElementExist(self.popup):
                self.click(self.confirm)
                self.type(self.phoneinput,phone)
                self.click(self.save)
            else:
                break
        else:
            print("号码重复超过10次")
            self.driver.quit()
        from UIpageApp.workWechatApp.addMemberPage import AddMemberPage
        return AddMemberPage(self.driver)

if __name__ == '__main__':
    print(CommonTools().phone())
    print(CommonTools().phone())