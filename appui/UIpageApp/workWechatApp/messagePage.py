from common.appUI_basic_operation import AppBasicOperation
from common.commonTools import CommonTools

data=CommonTools().get_test_data("workWechat")
import time
class MessagePage(AppBasicOperation):
    phonebook = data["messagePageApp"]["phonebook"]

    #跳转通讯录功能
    def go_to_phonebookPage(self):

        time.sleep(4)
        self.click(self.phonebook)
        time.sleep(4)
        from UIpageApp.workWechatApp.phonebookPage import PhoneBookPage
        return PhoneBookPage(self.driver)
