from common.appUI_basic_operation import AppBasicOperation
from common.commonTools import CommonTools

data=CommonTools().get_test_data("workWechat")
#添加页面
class AddMemberPage(AppBasicOperation):
    personadd=data["addMemberPageApp"]["personadd"]
    #去人工添加页面
    def go_to_personaddPage(self):
        self.click(self.personadd)
        from UIpageApp.workWechatApp.personAddPage import PersonBookPage
        return PersonBookPage(self.driver)


    #返回到通讯录页
    def back_to_phonebookPage(self):
        self.back()
        from UIpageApp.workWechatApp.phonebookPage import PhoneBookPage
        return PhoneBookPage(self.driver)