# Demo 用于加载不同的文件，对不同的文件作不同的处理
#下面每一个类，都有自己的实现的方法
#这个也是比较常用的写法。和工厂无关
class Demo:
    def load(self, rule):

        parse = None

        # 根据不同的 rule ，创建不同的对象

        if "xml" == rule:

            parse = XmlParse()

        elif "json" == rule:

            parse = JsonParse()

        elif "excel" == rule:

            parse = ExcelParse()

        elif "csv" == rule:

            parse = CsvParse()

        else:

            parse = OtherParse()

        # 调用对象的方法进行操作

        parse.parse()
# 相当于接口，用于规范各个解析类
# 每个解析类都要实现 parse 方法，否则在调用的时候就会报错
class IParse:

    def parse(self):

        raise ValueError()
class XmlParse(IParse):

    def parse(self):

        print("XmlParse")
class JsonParse(IParse):
    def parse(self):

        print("JsonParse")
class ExcelParse(IParse):

    def parse(self):

        print("ExcelParse")
class CsvParse(IParse):

    def parse(self):

        print("CsvParse")
class OtherParse(IParse):

    def parse(self):

        print("OtherParse")
if __name__ == "__main__":

    Demo().load("json")


