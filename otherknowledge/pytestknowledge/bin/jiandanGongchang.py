# Demo 用于加载不同的文件，对不同的文件作不同的处理
# 问题：如果创建对象的代码比如多，可能还会创建 text ，md，yml 等等
# 简单工厂解决：把对象的创建移动到其它类中， load 方法就会很简洁
class Demo:
    def load(self, rule):
        parse = ParseRuleFactory().create_parse(rule)
        # 调用对象的方法进行操作
        parse.parse()
# 简单工厂类：用于实例的创建，根据 rule 创建不同的实例。本质就是把 Demo 中原来创建实例的代码，给迁移过来
#这个就是工厂类，其实就是把，大量代码，封装了一个类里面
class ParseRuleFactory:
    #这里面是创建规则，最终return一个对象，这个对象，就根据你的规则，来定。比如，你传 json
    #就返回的 json对象。然后调用下面的各个对象自己的方法。如果是相同作用。方法名可以写成一个
    def create_parse(self, rule):

        parse = None

        # 根据不同的 rule ，创建不同的对象

        if "xml" == rule:

            parse = XmlParse()

        elif "json" == rule:

            parse = JsonParse()

        elif "excel" == rule:

            parse = ExcelParse()

        elif "csv" == rule:

            parse = CsvParse()

        else:

            parse = OtherParse()

        return parse
# 相当于接口，用于规范各个解析类
# 每个解析类都要实现 parse 方法，否则在调用的时候就会报错
class IParse:

    def parse(self):
        raise ValueError()
class XmlParse(IParse):

    def parse(self):
        print("XmlParse")
class JsonParse(IParse):

    def parse(self):
        print("JsonParse")
class ExcelParse(IParse):
    def parse(self):
        print("ExcelParse")
class CsvParse(IParse):

    def parse(self):
        print("CsvParse")
class OtherParse(IParse):

    def parse(self):
        print("OtherParse")
if __name__ == "__main__":
    Demo().load("json")