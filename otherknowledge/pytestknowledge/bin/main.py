import os
import pytest
from config.pathConfig import BASE_PATH
from common.commonTools import CommonTools
class Run:
    def __init__(self,env):
        CommonTools.env = env


    def pyrun(self,testcasepath,reportpath):
        #-n 10是10个并发，（前提下载插件）缩短时间，可以尝试，是否可以在selenium上运行。
        # 分布式用例设计原则
        # 用例独立，独立运行
        # 用例没有顺序，随机运行
        # 用例重复运行，运行结束即清理
        # 用例的参数化使用非动态数据（重点：下面讲）
        #pytest.main(["-v", "-s","-n","3", testcasepath, "--alluredir", reportpath])
        pytest.main(["-v", "-s", testcasepath, "--alluredir", reportpath])



if __name__ == '__main__':
    testcasepath=os.path.join(BASE_PATH,"testcase")
    reportpath=os.path.join(BASE_PATH,"report")
    Run("test").pyrun(testcasepath,reportpath)
    #执行用例，并把用例生成报告，path2是报告文件
    # pytest.main(["-v","-s",path1,"--alluredir",path2])
    #运行指定，feature.或者干脆直接命令行。
    # pytest.main(["-v","-s",path1,"--allure-features","计算器加法功能","--alluredir",path2])
    #指定多个feature运行
    #pytest.main(["-v", "-s", path1, "--allure-features", "计算器加法功能","--allure-features", "除法的事件", "--alluredir", path2])
    #指定stories运行
    #pytest.main(["-v", "-s", path1, " --allure-stories","int数据类型测试", "--alluredir", path2])
    # pytest.main(["-v", "-s", path1, "--allure-features", "计算器加法功能", "--alluredir", path2])
    #allure serve ./report  ,命令行，生产报告，其实是启动报告服务。去启动参数打开
    # cmd="allure serve "+reportpath
    #     # os.system(cmd)