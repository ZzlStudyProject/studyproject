from  abc import ABCMeta,abstractclassmethod,abstractmethod


#说明这是一个抽象的类metaclass=ABCMeta
class Anrmail(metaclass=ABCMeta):

    leixing=""

    @abstractmethod   #抽象方法.当你想让你子类，必须实现你的方法，就用这个注释
    def method1(self):
        pass

    def method2(self):
        pass

#抽象类，不能被实例化。
#可以实例化他的子类， 且必须实现抽象方法

