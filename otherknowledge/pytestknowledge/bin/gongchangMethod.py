# 问题：简单工厂不能解决创建实例的代码可能很复杂，即使迁移到了简单工厂中，复杂的创建过程依旧存在
# 解决：使用工厂方法，把创建过程封装到工厂类
class Demo:
    def load(self, rule):

        parse = None

        if "xml" == rule:

            # 省略 1000 行代码

            parse = XmlParse()

        elif "json" == rule:

            parse = JsonParseRuleFactory().create_parse()

        elif "excel" == rule:

            # 省略 1000 行代码

            parse = ExcelParse()

        elif "csv" == rule:

            # 省略 1000 行代码

            parse = CsvParse()

        else:

            # 省略 1000 行代码

            parse = OtherParse()

        # 调用对象的方法进行操作

        parse.parse()
# 相当于接口，用于规范各个工厂类
class IParseRuleFactory:
    def create_parse(self):
        raise ValueError()
# 工厂：把 Json 的解析放到此工厂下面
class JsonParseRuleFactory (IParseRuleFactory):
    def create_parse(self):
        # 省略 1000 行代码
        return JsonParse()

# 相当于接口，用于规范各个解析类
# 每个解析类都要实现 parse 方法，否则在调用的时候就会报错
class IParse:

    def parse(self):

        raise ValueError()

class XmlParse(IParse):

    def parse(self):

        print("XmlParse")

class JsonParse(IParse):

    def parse(self):

        print("JsonParse")

class ExcelParse(IParse):

    def parse(self):

        print("ExcelParse")

class CsvParse(IParse):

    def parse(self):

        print("CsvParse")

class OtherParse(IParse):

    def parse(self):

        print("OtherParse")

if __name__ == "__main__":

    Demo().load("json")