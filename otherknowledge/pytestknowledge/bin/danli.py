from threading import Lock


class Player:
    # 先定义一个，类变量（也叫全局变量，也叫静态变量）赋值为None  双下划线是私有的
    # 不能被修改（这个合理，单例模式可能不能就让别人随便就把这个改了）
    # python 的类变量会被多个类，实例共享
    __instane = None
    __flag = False

    def __new__(cls, *args, **kwargs):
        # 先调用父类的new方法，把cls（就是这个类自己）传给父类的new方法
        # 返回一个地址空间（就是实例化一个对象）。把这个地址空间（对象）赋值给
        # cls.__instane这个类的类变量。再返出去。

        # 这种写法，有个错误。你执行会发现，实例化后还是两个地址空间。
        # 这是肯定的。因为调用父类的方法。super().__new__(cls)每次都会生产不同的地址空间（对象）
        # object本身的new就是这个作用呀。你调用一次就新建一个（注意是新建，肯定是新地址空间）
        # 那要怎么写？写判断就行。if __instane=None 是none才会创建一个对象进行赋值。不是none直接返回
        # 这样实例化第一个对象时，才走父类方法，返回一个对象，实例化第二个时，__instane已经不是none。
        # 不走创建了，直接把这个对象返出去。这样永远都是一个对象了。
        if cls.__instane == None:
            cls.__instane = super().__new__(cls)
        return cls.__instane

    def __init__(self):
        if Player.__flag == False:
            print("当标识是flase才会执行，也就是实例第一个对象时。执行后，把标识改成true")
            # 修改的是类变量。全局的
            Player.__flag = True


class Player2:
    #引包，锁
    __instane_lock=Lock()
    __instane = None
    __id=1

    #懒汉式，本身不让去实例化
    def __new__(cls):
        raise ImportError ("实例不允许被创建")


    #用自己写的方法去实例化，调用的还是父类。因为不能new 一个Player2()对象。因为new对象，就走new，而不让走new了
    #不能实例化，所以你需要使用类方法，来实例化
    #自己的写的方法，可能存在并发问题，同时创建对象，而本身的 new是有python底层自身加锁保护的
    #你需要自己加锁
    @classmethod
    def get_instane(cls):
        #with 自己上锁，当发现A线程在运行中，就上锁
        with cls.__instane_lock:
            if cls.__instane==None:
                cls.__instane=super().__new__(cls)
        return cls.__instane

    def get_id(self):
        self.__id+=1
        print("实例去调用")
        return self.__id
if __name__ == '__main__':
    print(Player2.get_instane())
    print(Player2.get_instane().get_id())
    print(Player2.get_instane())
    print(Player2.get_instane().get_id())
