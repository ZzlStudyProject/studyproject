# 问题：如果多个公司都要封装工厂，比如 A, B, C ...公司都要封装自己的工厂，就要封装 n 个工厂类

# 解决：可以使用抽象工厂解决问题，每个工厂类可以创建多个实例，比如 JsonParseRuleFactory ，可以创建 A, B, C 公司的实例

# 一个工厂类，可以生成多个公司的解析方法

class IParseRuleFactory:

    def a_create_parse(self):
        raise ValueError()

    def b_create_parse(self):
        raise ValueError()

    def c_create_parse(self):
        raise ValueError()


# 实现时候，一个工厂类就可以生成多个公司的实例

class JsonParseRuleFactory(IParseRuleFactory):

    def a_create_parse(self):
        """

        """

    def b_create_parse(self):
        """

        """

    def c_create_parse(self):
        """

        """
