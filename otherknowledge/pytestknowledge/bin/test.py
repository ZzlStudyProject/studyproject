
#双指针,奇数在前，偶数在后
def doublePoint(arr):
    start=0
    end=len(arr)-1
    while start<end:
        if arr[start]%2==0:  #偶数放后面,做个交换
            x=arr[start]
            arr[start]=arr[end]
            arr[end]=x
            end=end-1
        else:
            start=start+1 #指针后移动一个

        if arr[end]%2!=0:  #是奇数前移动,下面的if是前后，提现两个指针
            y=arr[end]
            arr[end]=arr[start]
            arr[start]=y
            start=start+1
        else:
            end=end-1

#查找，二分法，arr是有序数组，n是目标数。你可以把arr变无序，考两个了，先排序，再查找。
#数组从下到大，或者从大到小。能说出原理也行。掐头去尾找中间，前后指针拉到中间，过滤一半数据
def half(arr,n):
    left=0
    right=len(arr)-1

    while left<=right:
        mid=(left+right)//2
        if n<arr[mid]:
            right=mid-1
        elif n>arr[mid]:
            left=mid+1
        elif n==arr[mid]:
            print("找到了")
            break
    else:
        print("可能不在数组里面")

import random
    #返回随机手机号
    def phone(self):
        prelist = ["130", "131", "132", "133", "134", "135", "136", "137", "138", "139", "147", "150", "151", "152",
                   "153",
                   "155", "156", "157", "158", "159", "186", "187", "188"]
        return random.choice(prelist) + "".join(random.choice("0123456789") for i in range(8))


    #返回随机姓名
    def random_name(self):
        headnamelist=["钱","孙","李","周","吴","郑","王","冯","陈","褚","卫","蒋","沈","韩","杨","朱","秦","尤","许"]
        tailnamelist=["秀","娟","英","华","慧","巧","美","娜","静","淑","惠","珠","翠","雅","芝","玉","萍","红","伟","刚","勇","毅","俊"]
        return random.choice(headnamelist)+"".join(random.choice(tailnamelist) for i in range(2))


if __name__ == '__main__':
    data={
  "result": {
    "cn": [
      {
        "android_channel": [
          "com.qihoo.appstore",
          "com.oppo.market",
          "com.heytap.market",
          "com.bbk.appstore",
          "com.noahedu.learnstore",
          "com.huawei.appmarket",
          "com.xiaomi.market",
          "com.tencent.android.qqdownloader",
          "com.eebbk.bbkmiddlemarket",
          "com.baidu.appsearch",
          "cn.dream.android.appstore",
          "com.meizu.mstore"
        ],
        "frame_start": 24,
        "app_scheme": "com.ihumand.yuwen",
        "subtitle": "学好阅读表达古诗成语",
        "frame_end": 47,
        "android_url": "",
        "appid": 18,
        "title": "系统语文AI互动课",
        "image": "https://config.ihumand.com/resource/image/18_CN_icon_iPhone_1620818558.svga",
        "image_key_frame": 33,
        "title_image": "",
        "icon_thumb": {
          "url": "https://config.ihumand.com/resource/app_icon/18_CN_thumb_iPhone_1619499116.png",
          "height": 120,
          "width": 120
        },
        "app_name": "洪恩语文课",
        "static_image": "https://config.ihumand.com/resource/static_image/18_CN_icon_iPhone_1620818558.png"
      },
      {
        "android_channel": [
          "com.qihoo.appstore",
          "com.oppo.market",
          "com.heytap.market",
          "com.bbk.appstore",
          "com.noahedu.learnstore",
          "com.huawei.appmarket",
          "com.xiaomi.market",
          "com.tencent.android.qqdownloader",
          "com.eebbk.bbkmiddlemarket",
          "com.baidu.appsearch",
          "cn.dream.android.appstore",
          "com.meizu.mstore"
        ],
        "frame_start": 27,
        "app_scheme": "com.ihuman.english",
        "subtitle": "启蒙教学口语训练课程",
        "frame_end": 56,
        "android_url": "",
        "appid": 6,
        "title": "3-12岁沉浸式儿童英语",
        "image": "https://config.ihumand.com/resource/image/6_CN_icon_iPhone_1581408317.svga",
        "image_key_frame": 33,
        "title_image": "https://config.ihumand.com/resource/title_image/6_CN_icon_iPhone_1581429946.png",
        "icon_thumb": {
          "url": "https://config.ihumand.com/resource/app_icon/6_CN_thumb_iPhone_1596703776.png",
          "height": 120,
          "width": 120
        },
        "app_name": "洪恩英语",
        "static_image": "https://config.ihumand.com/resource/static_image/6_CN_icon_iPhone_1581408317.png"
      }
    ],
    "en": [
      {
        "android_channel": [
          "com.qihoo.appstore",
          "com.oppo.market",
          "com.heytap.market",
          "com.bbk.appstore",
          "com.noahedu.learnstore",
          "com.huawei.appmarket",
          "com.xiaomi.market",
          "com.tencent.android.qqdownloader",
          "com.eebbk.bbkmiddlemarket",
          "com.baidu.appsearch",
          "cn.dream.android.appstore",
          "com.meizu.mstore"
        ],
        "frame_start": 24,
        "app_scheme": "com.ihumand.yuwen",
        "subtitle": "学好阅读表达古诗成语",
        "frame_end": 47,
        "android_url": "",
        "appid": 18,
        "title": "系统语文AI互动课",
        "image": "https://config.ihumand.com/resource/image/18_CN_icon_iPhone_1620818558.svga",
        "image_key_frame": 33,
        "title_image": "",
        "icon_thumb": {
          "url": "https://config.ihumand.com/resource/app_icon/18_CN_thumb_iPhone_1619499116.png",
          "height": 120,
          "width": 120
        },
        "app_name": "iHuman iReading",
        "static_image": "https://config.ihumand.com/resource/static_image/18_CN_icon_iPhone_1620818558.png"
      },
      {
        "android_channel": [
          "com.qihoo.appstore",
          "com.oppo.market",
          "com.heytap.market",
          "com.bbk.appstore",
          "com.noahedu.learnstore",
          "com.huawei.appmarket",
          "com.xiaomi.market",
          "com.tencent.android.qqdownloader",
          "com.eebbk.bbkmiddlemarket",
          "com.baidu.appsearch",
          "cn.dream.android.appstore",
          "com.meizu.mstore"
        ],
        "frame_start": 27,
        "app_scheme": "com.ihuman.english",
        "subtitle": "for kids aged 3–12",
        "frame_end": 56,
        "android_url": "",
        "appid": 6,
        "title": "Oral English training courses",
        "image": "https://config.ihumand.com/resource/image/6_EN_icon_iPhone_1581408455.svga",
        "image_key_frame": 33,
        "title_image": "https://config.ihumand.com/resource/title_image/6_EN_icon_iPhone_1581429959.png",
        "icon_thumb": {
          "url": "https://config.ihumand.com/resource/app_icon/6_EN_thumb_iPhone_1596703793.png",
          "height": 120,
          "width": 120
        },
        "app_name": "iHuman English",
        "static_image": "https://config.ihumand.com/resource/static_image/6_EN_icon_iPhone_1581408455.png"
      }
    ]
  },
  "code": 0
}

    print(type(data))
    print(str(data))
    print(type(str(data)))

    pytest
    pytest - ordering
    pytest - rerunfailures
    allure
    allure - pytest
    pip
    install
    pytest - ordering
    控制用例的执行顺序
    pip
    install
    pytest - dependency
    控制用例的依赖关系
    pip
    install
    pytest - xdist
    分布式并发执行测试用例(有时候报错，看看是不是没安装对应的包)
    pip
    install
    pytest - rerunfailures
    失败重跑
    pip
    install
    pytest - assume
    多重较验
    pip
    install
    pytest - random - order
    用例随机执行
    appium
    selenium
    mitmproxy == 5.2
    .0
    太高有问题代理
    要求python
    3.8
    faker
    生产动态数据，手机号，姓名等