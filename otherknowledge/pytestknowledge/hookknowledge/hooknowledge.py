


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item,call):
    """
    重新钩子函数，在用例执行时调用，setup，call，teardown 各调用一次，比如 setup失败了，就不走下面了
    上面的@pytest.hookimpl(tryfirst=True, hookwrapper=True)是pytest的插件，插件和hook函数是两个东西，hook函数是系统识别自动调用的
    #这个插件作用如下，你能获取执行结果是插件的功劳，插件作用在了这个函数，恰好这个函数是hook函数。
    1、该插件作用于pytest的钩子函数上，可以获取到测试用例不同执行阶段的结果（setup，call，teardown）
    2、可以获取钩子方法的调用结果（返回一个result对象）和调用结果的测试报告（返回一个report对象）
    :param item:用例
    :param call:用例步骤和结果
    :param out.get_result() 拿到测试报告集，后续做统计时也可以用到
    :return:
    """
    print('——————————————————————————')
    # 获取钩子方法的调用结果
    out = yield
    print('用例执行结果', out)
    print(GlobalConfig.DRIVER)
    driver=GlobalConfig.DRIVER
    # 3. 从钩子方法的调用结果中获取测试报告
    report = out.get_result()
    print("看下这是啥",report.failed)#report.failed 这是一个判断，对执行结果的是否失败了的判断，失败是true 成功是false
    print("这是步骤的执行结果？",report.outcome)
    print('测试报告：%s' % report)
    print('步骤：%s' % report.when)
    print('nodeid：%s' % report.nodeid)
    print("item:",item)
    print('call:%s' % call)
    if report.outcome == "failed":
        print("有失败，去截图")
        #传入的driver是采用全局变量的形式，写个全局变量的管理器文件，当用例初始化时，也赋值给全局变量，这样全局变量就是用例的driver
        #那么传入的driver是全局变量的driver，也就是用例的driver。保证了driver是同一个。截图时，就是同一个dirver截图（另起driver的话，截的是新driver图，完全不挨着了）
        #缺点，分布式时，不同用例同时在跑，你不能保证driver是哪一个。这是提供思路。全局变量，写成key value的形式。key是分布式用例的id标识
        #这个标识可以自己找，可以自己造，比如用例id，或者用例的名字，方法的的名字（errorScreen.__name__），或者使用pytest提供的('nodeid：%s' % report.nodeid)
        #都可以，看你实现
        BasicOperation(GlobalConfig.DRIVER).screenshot()
    elif report.outcome == "passed":
        print("成功的没必要截图，除非你要视频过程")
