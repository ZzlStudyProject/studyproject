# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:  conftest.py文件

#内置的插件，fixtrue装饰器的方法存储的文件。名称是固定的
#conftest.文件 路径位置，放在项目的全局的位置，（最好是根目录）。
# 如果每个项目需要自己的conftest.py，就在自己项目路径下创建。比如每个项目有自己的login而且重名了。
#conftest寻找规则。是在当前目录找，然后 一级一级往上翻


#fixture函数，可以不写在conftest.py文件里面，可以自己按照层级去创建文件夹。
#卸载conftest的好处是，不需要导包，直接用


from webui.common.webUI_basic_operation import BasicOperation
import pytest
from webui.config.globalconfig import GlobalConfig
import allure




@pytest.fixture(scope="module")
def module_fixture():
    print("使用fixture，module前置")
    # yield是生成器 相当于 return，第一次 执行会执行yield前的操作下次调用的时候，执行 yield后面的代码
    yield "代替成功"
    print("使用fixture，module后置")


@pytest.fixture(scope="class")
def setupAndteardown_class_fixture():
    print("使用fixture，class前置")
    yield "代替成功"
    print("使用fixture，class后置")

@pytest.fixture(autouse="true")
def myfixture():
    print(r"这是全局的引用autouse=“true”，不需要在某个方法调用，直接定义就行。收集要执行的所有用例执行前都会自动调用它")


#添加命令行参数函数，配合pytestconfig获取值一起使用。
def pytest_addoption(parser):
    #第一个参数name不一定是--，随便写，action 储存数值，default默认值。还有其他的值，看不太明白
    parser.addoption("--env",action="store",default="test",help="命令行注册环境参数")


#注册一个fixture函数，获取环境的。利用内置函数pytestconfig --env 后面传入的值。和上面的addoption配合使用
#因为是fixture函数，不能直接调用，想调用必须识别是test_*方法。想用这个根据命令行执行传参的来，来执行相应的用例
#那么公共函数，起名就要test开头.
@pytest.fixture()
def get_env(pytestconfig):
    return pytestconfig.getoption("--env")

#site-packages-_pytest-hookspec.py去找hook函数,每个hook函数看翻译啥作用。或者百度，每一个都能写出很丰富的实现功能。不同节点使用不同的hook函数
#pytest_collection_modifyitems在用例收集完毕之后被调用。session：会话对象；config：配置对象；items：用例对象列表；
def pytest_collection_modifyitems(session, config,items):
    for item in items:
        item.name = item.name.encode('utf-8').decode('unicode-escape')
        item._nodeid = item.nodeid.encode('utf-8').decode('unicode-escape')






@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item,call):
    """
    重构钩子函数，在用例执行时调用，setup，call，teardown 各调用一次，比如 setup失败了，就不走下面了
    :param item:用例
    :param call:用例步骤和结果
    :param out.get_result() 拿到测试报告集
    :return:
    """
    out = yield
    report = out.get_result()
    #不管是setup，call，teardown 失败都截图，所以就不再单独做判断步骤了
    if report.outcome == "failed":
        print("有失败，去截图")
        image=BasicOperation(GlobalConfig.DRIVER).screenshot()
        with open(image,"rb") as  f:
            file=f.read()
            allure.attach(file,"失败截图", allure.attachment_type.PNG)
    elif report.outcome == "passed":
        print("成功的没必要截图，除非你要视频过程")


