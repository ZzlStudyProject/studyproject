import pytest
import yaml

@pytest.fixture()#加上这个装饰器，就可以别处调用前置了
def login():
    cookie="aabbcc"
    return cookie

class TestPytest():

    #参数化，必须掌握，注意语法，a b 参数是要放在 一个 “”  里面 用 逗号 隔开的。list里面是，几组数据，几组数据，就生成几个用例
    @pytest.mark.parametrize("a,b",[(1,1),(3,6),(5,5),("zhangsan","zhangsan"),("张三","李四")])
    def test_pytest01(self,a,b):
        assert a==b

    #login 就可以当参数，传递，意思先执行，这个方法。如果有返回值的话，你还可以接收
    def test_pytest02(self,login):
        x='abc'
        print(f"{login}")
        assert hasattr(x,'d')

    @pytest.mark.parametrize("a,b",yaml.safe_load(open("./data.yml")))
    def test_pytest03(self,a,b):
        assert a==b

