import pytest

def setup_module():
    print("\nsetup_module,只执行一次，当多个测试类时的时候使用")


def teardown_module():
    print("\nteardown_module,只执行一次，当多个测试类时的时候使用")


#为什么 要继承 object 其实就是继承之后，有些可以直接调用 object这个大类的方法，你完全可以点进去看下，object里面有哪些方法
class TestPytest1(object):
    @classmethod
    def setup_class(cls):
        print("\nsetup_class,只执行一次")

    @classmethod     #@classmethod是类方法装饰器，作用 ，可以直接使用类.方法调用
    def teardown_class(cls):
        print("\ndeardown_class,只执行一次")

    def setup_method(self):
        print("\n setup_method,每个测试方法都执行一次")

    def teardown_method(self):
        print(r"teardown_method,每个测试方法都执行一次")

    def test_three(self):
        print("这是测试用例，test_three")

    def test_four(self):
        print("test_four,测试用例")

class TestPytest2(object):
        @classmethod
        def setup_class(cls):
            print("\nsetup_class2,只执行一次")

        @classmethod
        def teardown_class(cls):
            print("\nteardown_class2,只执行一次")

        def setup_method(self):
            print("\nsetup_method2,每次测试方法都执行一次")

        def teardown_method(self):
            print("\nteardown_method2,每个测试方法都执行一次")

        @pytest.mark.run(order=1)
        def test_two(self):
            print("test_two,测试用例,第二个执行")

        @pytest.mark.run(order=2)
        def test_one(self):
            print("test_one,测试用例，第一个执行")





