import random
a="abcdefghi\n1234"

def test01(list):
    for i in range(1,6):
        list.append(i**2)
    print(list)

def test02(list):
    for i in range(1,6):
        if i==4:
            list.append(i)


if __name__ == '__main__':
    list=[]
    # test01(list)
    #列表链式推导，  前面是要执行的，后面是循环或者条件， 所以先写后面
    # list=[i**2 for i in range(1,6)]
    # print(list)
    list=[i for i in range(1,6) if i==4]
    #print(list)

    tupe1=(1,2,3,"a","a")
    #print(tupe1.count("a")) #个数
    #print(tupe1.index("a"))#第一个下标数

    a={1,2,6}  # 要赋值，不赋值是字典类型
    c=set()  #空集合这样定义  集合类型是 set
    b={1,3,3,5,7}
    a.add("a") #添加
    print(a.union(b)) #并集
    print(a.intersection(b)) #交集
    print(a.difference(b))#差集，a有b没有
    print(a.clear()) #清空
    print(set(b)) #去重


    dict1={"a":1,"b":2}  #key value形式
    dict2=dict(a=1,b=2)

    print(dict1.keys())
    print(dict1.values())
    print(dict1.pop("a"))#删除 键值对  a,pop返回对应的vlaue
    print(dict1)
    print(dict2.popitem())#随机删除一个键值对，并返回 这个键值对,返回的是元组
    print(dict2)

    dict3={}
    dict4 = {}
    print(dict3.fromkeys(("a","c","f"),"a"))#赋key的值
    dict4.fromkeys("a","c","f")
    print(dict3)
    print(dict4)