import threading #更高级，在_thread基础上的
import logging
from time import time,ctime


def loop0():
    logging.info("开始执行loop0",ctime)
    time.sleep(4)
    logging.info("已经执行完毕loop0", ctime)


def loop1():
    logging.info("开始执行loop1",ctime)
    time.sleep(2)
    logging.info("已经执行完毕loop1", ctime)

def loop():
    logging.info("开始执行loop",ctime)
    loop0()
    loop1()
    logging.info("已经执行完毕loop", ctime)





if __name__ == '__main__':
    loop()