import time

#这是一种思路，
class Screen:
    def __init__(self,driver):
        self.driver=driver

    def __call__(self,func):
        def inner(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except:
                now_time = time.strftime('%Y_%m_%d_%H_%M_%S')  # 异常时，截图
                self.driver.get_screenshot_as_file(f'{now_time}.png')
                raise  # 抛出异常，不然会认为测试用例执行通过




from webui.config.pathConfig import BASE_PATH
import datetime
import pytest
from webui.common.commonTools import CommonTools
data = CommonTools().get_test_data(path='workWechat', file='testparamData.yml')
fakerRamdomData = CommonTools().faker_random()


# ui自动化截图的装饰。开始是用闭包实现截图 但是用点小问题。放弃了，使用了hook函数实现
#问题1，闭包和pytest参数化装饰器，结合使用。参数化的数据传不进去闭包函数。单独使用可以
#问题2，闭包的装饰在setup是不好使的，setup才开始实例driver，而闭包是在setup前传入driver，这时还没有呢，所以无法截图setup
def errorScreen(func):
    def wrapper(self, *args, **kwargs):
        try:
            func(self,*args, **kwargs)
        except:
            filePath = BASE_PATH + "\\report\\images\\"  # 设置存放截图的路径。一定要有\\report\\images\\这个文件夹，没有创建去。或者写个创建代码
            nowtime = datetime.datetime.now().strftime('%Y%m%d%H%M%S')  # 格式化时间
            # 定义截图文件名称 前面的filePath是一定要写的，是存放路径。get_screenshot_as_file方法就这样的。识别路径存放
            imageName = filePath + nowtime + '.png'
            # 调用自带的截图函数
            self.driver.get_screenshot_as_file(imageName)
            raise  # 抛出异常，不然会认为测试用例执行通过。try:xx 有问题，走except但是程序每段，pytest认为用例是通过的
        # 所以要主动抛出异常



import sys, os
#添加到默认路径，python有时默认路径出问题，导致运行时提示缺少引包的模块,且要放在导包的前面
BASE_PATH = os.path.abspath(os.path.join(os.getcwd(), "../../.."))
sys.path.append(BASE_PATH)


if __name__ == '__main__':
    print(errorScreen.__name__)  #打印函数名字
    print(BASE_PATH)
    #几种标准写法
    "C:/Users/admin/Desktop/file_shot"
    "C:\\Users\\admin\\Desktop\\file_shot"
    r"C:\Users\admin\Desktop\file_shot"
