class BasicOperation:

    def __init__(self,base_driver:webdriver=None):
        """
        每个page类都继承基类，那只要加载每个page对象，都会走基类的init方法，都会重新实例化一个driver
        每个page方法里面的return时，直接把driver，传递下去 如: return AddMemberPage(self.driver)
        解决，driver多次实例化，多次启动窗口问题
        init函数是不可以return的
        :param base_driver: base_driver:webdriver 这样的写法，是赋值类型，再赋值None 赋值类型含义，后面再调用时，.方法方便
        """
        if base_driver == None:
            options = webdriver.ChromeOptions()
            options.add_argument('--ignore-certificate-errors')
            self.driver = webdriver.Chrome(options=options)
            self.driver.implicitly_wait(10)
            self.driver = base_driver
        else:
            self.driver=base_driver