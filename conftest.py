# -*- coding: utf-8 -*-
# @CreateTime    : 2021/6/16
# @CreateAuthor  : zhangzhenglai
# @UpdateTime    : 2021/6/16 16:43
# @UpdateAuthor  : zhangzhenglai
# @Function:  conftest.py文件



from webui.common.webUI_basic_operation import BasicOperation
import pytest
from webui.config.globalconfig import GlobalConfig
import allure



@pytest.fixture(autouse="true")
def myfixture():
    print(r"这是全局的引用autouse=“true”，不需要在某个方法调用，直接定义就行。收集要执行的所有用例执行前都会自动调用它")


#添加命令行参数函数，配合pytestconfig获取值一起使用。
def pytest_addoption(parser):
    parser.addoption("--env",action="store",default="test",help="命令行注册环境参数")



@pytest.fixture()
def get_env(pytestconfig):
    return pytestconfig.getoption("--env")


def pytest_collection_modifyitems(session, config,items):
    """
    解决 pytest执行结果中文乱码
    :param session: 会话对象；
    :param config:  配置对象；
    :param items: 用例对象列表；
    :return:
    """
    for item in items:
        item.name = item.name.encode('utf-8').decode('unicode-escape')
        item._nodeid = item.nodeid.encode('utf-8').decode('unicode-escape')






@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item,call):
    """
    重构钩子函数，在用例执行时调用，setup，call，teardown 各调用一次
    :param item:用例
    :param call:用例步骤和结果
    :param out.get_result() 拿到测试报告集
    :return:
    """
    out = yield
    report = out.get_result()
    #不管是setup，call，teardown 失败都截图，所以就不再单独做判断步骤了
    if report.outcome == "failed":
        print("有失败，去截图")
        image=BasicOperation(GlobalConfig.DRIVER).screenshot()
        with open(image,"rb") as  f:
            file=f.read()
            allure.attach(file,"失败截图", allure.attachment_type.PNG)
    elif report.outcome == "passed":
        print("成功的没必要截图，除非你要视频过程")


